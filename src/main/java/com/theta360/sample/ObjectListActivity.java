package com.theta360.sample;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.UploadTask;
import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.entity.*;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;
import com.theta360.lib.ptpip.settingvalue.ISOSpeed;
import com.theta360.lib.ptpip.settingvalue.ShutterSpeed;
import com.theta360.sample.view.LogView;
import com.theta360.sample.view.ObjectListArrayAdapter;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.net.SocketFactory;
import javax.net.ssl.HttpsURLConnection;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


/**
 * Activity that displays the photo list
 */
public class ObjectListActivity extends FragmentActivity {
    private ListView objectList;
    private LogView logViewer;
    private String cameraIpAddress;
    //test
    private LinearLayout layoutCameraArea;
    private Button btnShoot, btnUpload;
    private TextView textCameraStatus;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String mUserId;
    private SignInButton signInButton;
    private Button signOutButton;

    private String itemsUrl;
    private ArrayList<String> names;
    private ArrayList<Integer> objlist, slist;
    private int i;
    private int x = 0;
    private ListView lv, lv1;
    private ObjectHandles objectHandles;
    private TextView mainText, testText;
    private WifiManager mainWifi;
    private List<ScanResult> wifiList;
    private StringBuilder sb;
    private byte[] image;
    private byte[] realImageTest;
    private WifiManager wifiManager;
    private String networkPass;
    private String networkSSID;
    private ObjectRow objectRow;
    private ObjectRow objectRow2;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private UploadTask uploadTask;
    private UploadTask uploadTask2;
    List<ObjectRow> objectRows2;
    private String ssIdTHETA;
    private String passTHETA;
    private WifiManager wifi;
    private List<ScanResult> results;
    private MyReceiver myReceiver;
    private int camCount = 0;
    private boolean isAlertDialogShown = false;
    private String lnUrl;
    private String shUrl;
    private Context context;
    private Button viewBut;
    private List<String> putKeyStr;
    private List<String> putKeyScene;
    private Button btnView;
    private List<String> thumbUrl;
    private List<String> realUrl;
    private String fUID;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase mDatabase;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    SeekBar seekBarISO;
    SeekBar seekBarShutSP;
    TextView isoVal;
    TextView shutVal;
    Switch shutSound;
    boolean volu;
    ShutterSpeed posShut;
    ISOSpeed posIso;

    /**
     * onCreate Method
     *
     * @param savedInstanceState onCreate Status value
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_list);
        context = this.getApplicationContext();
        objlist = new ArrayList<Integer>();
        btnUpload = (Button) findViewById(R.id.btn_upload);
        logViewer = (LogView) findViewById(R.id.log_view);
        btnView = (Button) findViewById(R.id.buttonView);
        shutSound = (Switch) findViewById(R.id.switchShSnd);
        names = new ArrayList<String>();
        seekBarISO = (SeekBar) findViewById(R.id.seekBarISO);
        seekBarShutSP = (SeekBar) findViewById(R.id.seekBarShutSP);
        isoVal = (TextView) findViewById(R.id.textViewISOValue);
        shutVal = (TextView) findViewById(R.id.textViewShutSPVal);
        shutSound.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    volu = true;
                } else {
                    volu = false;
                }
            }
        });
        seekBarISO.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int pos, boolean b) {
                if (seekBar.equals(seekBarISO)) {
                    switch (pos) {
                        case 0:
                            posIso = ISOSpeed.AUTO;
                            isoVal.setText("AUTO");
                            break;
                        case 1:
                            posIso = ISOSpeed.ISO100;
                            isoVal.setText("100");
                            break;
                        case 2:
                            posIso = ISOSpeed.ISO125;
                            isoVal.setText("125");
                            break;
                        case 3:
                            posIso = ISOSpeed.ISO160;
                            isoVal.setText("160");
                            break;
                        case 4:
                            posIso = ISOSpeed.ISO200;
                            isoVal.setText("200");
                            break;
                        case 5:
                            posIso = ISOSpeed.ISO250;
                            isoVal.setText("250");
                            break;
                        case 6:
                            posIso = ISOSpeed.ISO320;
                            isoVal.setText("320");
                            break;
                        case 7:
                            posIso = ISOSpeed.ISO400;
                            isoVal.setText("400");
                            break;
                        case 8:
                            posIso = ISOSpeed.ISO500;
                            isoVal.setText("500");
                            break;
                        case 9:
                            posIso = ISOSpeed.ISO640;
                            isoVal.setText("640");
                            break;
                        case 10:
                            posIso = ISOSpeed.ISO800;
                            isoVal.setText("800");
                            break;
                        case 11:
                            posIso = ISOSpeed.ISO1000;
                            isoVal.setText("1000");
                            break;
                        case 12:
                            posIso = ISOSpeed.ISO1250;
                            isoVal.setText("1250");
                            break;
                        case 13:
                            posIso = ISOSpeed.ISO1600;
                            isoVal.setText("1600");
                            break;
                        default:
                            posIso = ISOSpeed.AUTO;
                            isoVal.setText("DEFAULT");
                            break;
                    }
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarShutSP.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int pos, boolean b) {
                if (seekBar.equals(seekBarShutSP)) {
                    switch (pos) {
                        case 0:
                            posShut = ShutterSpeed.AUTO;
                            shutVal.setText("AUTO");
                            break;
                        case 1:
                            posShut = ShutterSpeed.SPEED1_8000;
                            shutVal.setText("1/8000");
                            break;
                        case 2:
                            posShut = ShutterSpeed.SPEED1_6400;
                            shutVal.setText("1/6400");
                            break;
                        case 3:
                            posShut = ShutterSpeed.SPEED1_2000;
                            shutVal.setText("1/2000");
                            break;
                        case 4:
                            posShut = ShutterSpeed.SPEED1_1000;
                            shutVal.setText("1/1000");
                            break;
                        case 5:
                            posShut = ShutterSpeed.SPEED1_500;
                            shutVal.setText("1/500");
                            break;
                        case 6:
                            posShut = ShutterSpeed.SPEED1_250;
                            shutVal.setText("1/250");
                            break;
                        case 7:
                            posShut = ShutterSpeed.SPEED1_125;
                            shutVal.setText("1/125");
                            break;
                        case 8:
                            posShut = ShutterSpeed.SPEED1_50;
                            shutVal.setText("1/50");
                            break;
                        case 9:
                            posShut = ShutterSpeed.SPEED1_25;
                            shutVal.setText("1/25");
                            break;
                        case 10:
                            posShut = ShutterSpeed.SPEED1_10;
                            shutVal.setText("1/10");
                            break;
                        default:
                            posShut = ShutterSpeed.AUTO;
                            shutVal.setText("DEFAULT");
                            break;

                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        firebaseAuth = FirebaseAuth.getInstance();
        fUID = firebaseAuth.getCurrentUser().getUid();
        itemsUrl = "https://nextweaverproject.firebaseio.com/users/" + fUID;
        mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = mDatabase.getReferenceFromUrl(itemsUrl);
        thumbUrl = new ArrayList<>();
        realUrl = new ArrayList<>();
        myRef.child("Scenes").child("Scene").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot children : dataSnapshot.getChildren()) {
                    for (DataSnapshot child : children.getChildren()) {
                        //Log.v("key1","   " + child.getKey());
                        if (child.getKey().equals("Thumb")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                            thumbUrl.add(temp);
                                        }

                                    }
                                }
                            }
                        }
                        if (child.getKey().equals("Real")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4R", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                            realUrl.add(temp);
                                        }

                                    }
                                }
                            }
                        }
                    }


                    // Log.v("key2","   " + thumbUrl);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (thumbUrl != null) {
            Log.v("keyResult", "   " + thumbUrl + "  " + thumbUrl.size());
        } else Log.v("keyResult", "   " + thumbUrl + "  ");
        mRecyclerView = (RecyclerView) findViewById(R.id.object_list);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        btnView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), FromFirebaseActivity.class);
                intent.putExtra("thumbUrl", new ArrayList<String>(thumbUrl));
                intent.putExtra("realUrl", new ArrayList<String>(realUrl));
                Log.v("keyResultIntent", "   " + thumbUrl + "  " + thumbUrl.size());
                startActivity(intent);
            }
        });
//---------------------------------********************* UPLOAD ******************------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------
        mAuth = FirebaseAuth.getInstance();
        viewBut = (Button) findViewById(R.id.buttonView);
//		mUserId = mAuth.getCurrentUser().getUid().toString();
        itemsUrl = "https://nextweaverproject.firebaseio.com/users/" + mUserId;
        cameraIpAddress = getResources().getString(R.string.theta_ip_address);
        getActionBar().setTitle(cameraIpAddress);
        layoutCameraArea = (LinearLayout) findViewById(R.id.shoot_area);
        textCameraStatus = (TextView) findViewById(R.id.camera_status);
        btnShoot = (Button) findViewById(R.id.btn_shoot);
        btnShoot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                btnShoot.setEnabled(false);
                textCameraStatus.setText(R.string.text_camera_synthesizing);
                new ShootTask(posIso, posShut, volu).execute();

            }
        });


    }

    /**
     * onCreateOptionsMenu Method
     *
     * @param menu Menu initialization object
     * @return Menu display feasibility status value
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.connection, menu);

        Switch modeSwitch = (Switch) menu.findItem(R.id.connection).getActionView().findViewById(R.id.mode_switch);
        modeSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if (isChecked) {
                    // CAMERA
                    connectCamWifi();

                } else {
                    // NETWORK
                    reconnectWifi();

                }
            }
        });

        Switch connectionSwitch = (Switch) menu.findItem(R.id.connection).getActionView().findViewById(R.id.connection_switch);
        connectionSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            LoadObjectListTask sampleTask = new LoadObjectListTask();

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

//				MyAdapter nullAdapter = new MyAdapter(null);
//				mRecyclerView.setAdapter(nullAdapter);

                if (isChecked) {
                    layoutCameraArea.setVisibility(View.VISIBLE);
                    sampleTask.execute();
                } else {
                    layoutCameraArea.setVisibility(View.INVISIBLE);
                    sampleTask.cancel(true);
                    sampleTask = new LoadObjectListTask();
                    new DisConnectTask().execute();
                }
            }
        });
        return true;
    }

    private void changeCameraStatus(final int resid) {
        runOnUiThread(new Runnable() {
            public void run() {
                textCameraStatus.setText(resid);
            }
        });
    }

    private void appendLogView(final String log) {
        runOnUiThread(new Runnable() {
            public void run() {
                logViewer.append(log);
            }
        });
    }

    private class LoadObjectListTask extends AsyncTask<Void, String, List<ObjectRow>> {

        public LoadObjectListTask() {

        }

        @Override
        protected void onPreExecute() {
            startAnim();
        }

        @Override
        protected List<ObjectRow> doInBackground(Void... params) {
            try {
                publishProgress("------");
                publishProgress("connecting to " + cameraIpAddress + "...");
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);
                publishProgress("connected.");
                changeCameraStatus(R.string.text_camera_standby);
                DeviceInfo deviceInfo = camera.getDeviceInfo();
                publishProgress(deviceInfo.getClass().getSimpleName() + ":<" + deviceInfo.getModel() + ", " + deviceInfo.getDeviceVersion() + ", " + deviceInfo.getSerialNumber() + ">");
                objectHandles = camera.getObjectHandles(PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT);
                int objectHandleCount = objectHandles.size();
                publishProgress("getObjectHandles() received " + objectHandleCount + " handles.");
                List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
                objectRows2 = new ArrayList<ObjectRow>();
                StorageIds storageIDs = camera.getStorageIDs();
                int storageCount = storageIDs.size();
                for (int i = 0; i < storageCount; i++) {
                    int storageId = storageIDs.getStorageId(i);
                    StorageInfo storage = camera.getStorageInfo(storageId);
                    ObjectRow storageCapacity = new ObjectRow();
                    int freeSpaceInImages = storage.getFreeSpaceInImages();
                    int megaByte = 1024 * 1024;
                    long freeSpace = storage.getFreeSpaceInBytes() / megaByte;
                    long maxSpace = storage.getMaxCapacity() / megaByte;
                    storageCapacity.setFileName("Free space: " + freeSpaceInImages + "[shots] (" + freeSpace + "/" + maxSpace + "[MB])");
                    objectRows.add(storageCapacity);

                }
                for (i = 0; i < objectHandleCount; i++) {
                    objectRow = new ObjectRow();
                    objectRow2 = new ObjectRow();
                    final int objectHandle = objectHandles.getObjectHandle(i);
                    ObjectInfo object = camera.getObjectInfo(objectHandle);
                    ObjectInfo object2 = camera.getObjectInfo(objectHandle);
                    objectRow.setObjectHandle(objectHandle);
                    objectRow.setFileName(object.getFilename());
                    objectRow.setCaptureDate(object.getCaptureDate());
                    objectRow2.setObjectHandle(objectHandle);
                    objectRow2.setFileName(object2.getFilename());
                    objectRow2.setCaptureDate(object2.getCaptureDate());
                    if (object.getObjectFormat() == ObjectInfo.OBJECT_FORMAT_CODE_EXIF_JPEG) {
                        objectRow2.setIsPhoto(true);
                        PtpObject realPic = camera.getResizedImageObject(objectHandle, 2048, 1024);
                        final byte[] realImage = realPic.getDataObject();
                        objectRow2.setThumbnail(realImage);
                        objectRow.setIsPhoto(true);
                        PtpObject thumbnail = camera.getThumb(objectHandle);
                        final byte[] thumbnailImage = thumbnail.getDataObject();
                        objectRow.setThumbnail(thumbnailImage);

                    } else {
                        objectRow.setIsPhoto(false);
                        objectRow2.setIsPhoto(false);

                    }
                    if (i == 0) {
                        ObjectRow empty = new ObjectRow();
                        objectRows2.add(empty);
                        objectRows2.add(objectRow2);
                    } else {
                        objectRows2.add(objectRow2);
                    }
                    objectRows.add(objectRow);


                    objlist.add(i + 1);
                    publishProgress("getObjectInfo: " + (i + 1) + "/" + objectHandleCount);
                }
                return objectRows;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                logViewer.append(log);
            }
        }

        @Override
        protected void onPostExecute(final List<ObjectRow> objectRows) {
            if (objectRows != null) {
                //final ObjectListArrayAdapter objectListArrayAdapter = new ObjectListArrayAdapter(ObjectListActivity.this, R.layout.listlayout_object, objectRows);
                people = new ArrayList<>();
                Log.v("Err", String.valueOf(objectRows.size()));
                for (int i = 3; i < objectRows.size(); i++) {
                    byte[] test = objectRows.get(i).getThumbnail();
                    Bitmap thumbPic = BitmapFactory.decodeByteArray(test, 0, test.length);
                    String thumbName = objectRows.get(i).getFileName().toString();
                    people.add(new PersonData(
                            thumbName,
                            thumbPic
                    ));
                }
                adapter = new MyAdapter(people);
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(400);
                mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));
                ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                        byte[] thumbnail = objectRows.get(position + 3).getThumbnail();
                        int objectHandle = objectRows.get(position + 3).getObjectHandle();
                        GLPhotoActivity.startActivity(ObjectListActivity.this, cameraIpAddress, objectHandle, thumbnail);
                        // do it
                    }
                });
                /*objectList.setOnItemClickListener(new OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						ObjectRow selectedItem = (ObjectRow) parent.getItemAtPosition(position);
						if (selectedItem.isPhoto()) {
/*//*******************************************----------------------- OBJECTROW SELECTED ---------------------------------------------***********************************

                 byte[] thumbnail = selectedItem.getThumbnail();
                 int objectHandle = selectedItem.getObjectHandle();
                 GLPhotoActivity.startActivity(ObjectListActivity.this, cameraIpAddress, objectHandle, thumbnail);


                 } else {
                 Toast.makeText(getApplicationContext(), "This isn't a photo.", Toast.LENGTH_SHORT).show();
                 }
                 }
                 });*/


            }

            btnUpload.setOnClickListener(new OnClickListener() {
                String shortUr;
                String longUr;

                @Override
                public void onClick(View v) {

                    Intent intent2 = new Intent(getApplicationContext(), UploadActivity.class);
                    reconnectWifi();
                    // Bundle args = new Bundle();
                    // args.putParcelable("latlng", latLng);
                    //intent2.putExtra("bundle", args);
                    mUserId = mAuth.getCurrentUser().getUid().toString();
                    itemsUrl = "https://nextweaverproject.firebaseio.com/users/" + mUserId;
                    Log.v("RektUP", "GG  long URL " + lnUrl);
                    Log.v("RektUP", "GG short URL " + shUrl);
                    Log.v("RektUP", "Item URL " + itemsUrl);
                    FirebaseDatabase database = FirebaseDatabase.getInstance();
                    final DatabaseReference myRef = database.getReferenceFromUrl(itemsUrl);
                    final String putKeyScene = myRef.child("Scenes").child("Scene").push().getKey();
                    myRef.child("Scenes").child("KeyScene").child(putKeyScene).setValue(putKeyScene);
                    myRef.child("Scenes").child("Scene").child(putKeyScene).child("SceneName").setValue("Name");
                    putKeyStr = new ArrayList<String>();
                    storage = FirebaseStorage.getInstance();
                    // Create a storage reference from our app
                    storageRef = storage.getReferenceFromUrl("gs://nextweaverproject.appspot.com/TestPicTheta/" + mUserId);
                    // --------------------------- CHOOSE FILE TO UPLOAD HERE ------------------------------------ //
                    int obj1 = objectRows.size();
                    int obj2 = objectRows2.size();
                    Log.v("UPLOADED TO", "Up1         " + obj1);
                    Log.v("UPLOADED TO", "up2         " + obj2);
                    for (int z = 0; z < obj1; z++) {
                        final String putKeyPic = myRef.child("Scenes").child("Scene").child(putKeyScene).child("Thumb").push().getKey();
                        putKeyStr.add(putKeyPic);
                        if (z > 2) {
                            myRef.child("Scenes").child("KeyPic").child(putKeyStr.get(z)).setValue(putKeyStr.get(z));
                        }

                        final ObjectRow upload1 = objectRows.get(z);
                        // set file upload destination
                        Log.v("UPLOADED TO", "Can get Obj1");
                        if (upload1.isPhoto()) {
                            StorageReference riversRef = storageRef.child("images/" + upload1.getFileName() + ".jpg");
                            image = upload1.getThumbnail();
                            uploadTask = riversRef.putBytes(image);

                            final int finalZ2 = z;
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                    Log.v("UPLOADED TO", "Fail" + i + ".JPG");
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    String dlUrl = downloadUrl.toString();
                                    Log.v("UPLOADED TO", "Upload Photo 1  " + finalZ2 + ". " + upload1.getFileName());
                                    Log.v("UPLOADED TO", "Upload Photo 1  " + finalZ2 + ". " + upload1.getFileName() + "short url :  " + shortUr);
                                    Log.v("UPLOADED TO", "Upload Photo 1  " + finalZ2 + ". " + upload1.getFileName() + "long url :  " + dlUrl);
                                    Map<String, String> post3 = new HashMap<String, String>();
                                    post3.put("id", String.valueOf(finalZ2 - 2));
                                    post3.put("PicName", upload1.getFileName());
                                    post3.put("LongUrl", dlUrl);
                                    post3.put("Location", "sumware" + String.valueOf(finalZ2));
                                    myRef.child("Scenes").child("Scene").child(putKeyScene).child("Thumb").child(putKeyPic).setValue(post3);
                                    myRef.child("Scenes").child("Scene").child(putKeyScene).child("SceneId").setValue(finalZ2);

                                }
                            });

                        } else Log.v("UPLOADED TO", "Not Photo OBj1   " + z + ".");
                    }
                    for (int z = 0; z < obj1; z++) {
                        final ObjectRow upload2 = objectRows2.get(z);
                        Log.v("UPLOADED TO", "Can get Obj2");
                        if (upload2.isPhoto()) {
                            StorageReference riversRefReal = storageRef.child("RealPic/" + upload2.getFileName() + ".jpg");
                            realImageTest = upload2.getThumbnail();
                            uploadTask2 = riversRefReal.putBytes(realImageTest);
                            final int finalZ = z;
                            final int finalZ1 = z;
                            final int finalZ2 = z;
                            uploadTask2.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle unsuccessful uploads
                                    Log.v("UPLOADED TO", "Fail t2" + i + ".JPG");
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                    Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                    String dlUrl = downloadUrl.toString();
                                    Log.v("UPLOADED TO", "Upload Photo  2 " + finalZ1 + ". " + upload2.getFileName());
                                    Log.v("UPLOADED TO", "Upload Photo  2 " + finalZ1 + ". " + upload2.getFileName() + "short url :  " + shortUr);
                                    Log.v("UPLOADED TO", "Upload Photo  2 " + finalZ1 + ". " + upload2.getFileName() + "long url :  " + dlUrl);
                                    // set file upload destination
                                    Map<String, String> post4 = new HashMap<String, String>();
                                    post4.put("id", String.valueOf(finalZ1 - 2));
                                    post4.put("PicName", upload2.getFileName());
                                    post4.put("LongUrl", dlUrl);
                                    post4.put("Location", "sumware" + String.valueOf(finalZ1));
                                    myRef.child("Scenes").child("Scene").child(putKeyScene).child("Real").child(putKeyStr.get(finalZ2)).setValue(post4);
                                }
                            });

                        } else Log.v("UPLOADED TO", "Not Photo OBj2   " + z + ".");
                    }
                    startActivity(intent2);


                }


                // Register observers to listen for when the download is done or if it fails
            });
            stopAnim();
        }

        @Override
        protected void onCancelled() {
            stopAnim();
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TAG", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            //mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
            Log.d("TAG", "Disp name:" + acct.getDisplayName());
            //updateUI(true);
        } else {
            // Signed out, show unauthenticated UI.
            //updateUI(false);
        }
    }

    private class DisConnectTask extends AsyncTask<Void, String, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                PtpipInitiator.close();
                publishProgress("disconnected.");
                return true;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return false;
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                logViewer.append(log);
            }
        }
    }

    private static enum ShootResult {
        SUCCESS, FAIL_CAMERA_DISCONNECTED, FAIL_STORE_FULL, FAIL_DEVICE_BUSY
    }

    private class ShootTask extends AsyncTask<Void, Void, ShootResult> {
        private ISOSpeed isoPos;
        private ShutterSpeed shutSpPos;
        private boolean vol;


        public ShootTask(ISOSpeed isoPosition, ShutterSpeed shutSpPosition, boolean volume) {
            isoPos = isoPosition;
            shutSpPos = shutSpPosition;
            vol = volume;
        }

        @Override
        protected void onPreExecute() {
            logViewer.append("initiateCapture");
        }

        ;

        @Override
        protected ShootResult doInBackground(Void... params) {
            CaptureListener postviewListener = new CaptureListener();
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), getResources().getString(R.string.theta_ip_address));
                if (vol) {
                    camera.setAudioVolume(50);
                } else {
                    camera.setAudioVolume(0);
                }


                camera.setShutterSpeed(shutSpPos);
                camera.setExposureIndex(isoPos);
                camera.initiateCapture(postviewListener);
                return ShootResult.SUCCESS;

            } catch (IOException e) {
                return ShootResult.FAIL_CAMERA_DISCONNECTED;
            } catch (ThetaException e) {
                if (Response.RESPONSE_CODE_STORE_FULL == e.getStatus()) {
                    return ShootResult.FAIL_STORE_FULL;
                } else if (Response.RESPONSE_CODE_DEVICE_BUSY == e.getStatus()) {
                    return ShootResult.FAIL_DEVICE_BUSY;
                } else {
                    return ShootResult.FAIL_CAMERA_DISCONNECTED;
                }
            }
        }

        @Override
        protected void onPostExecute(ShootResult result) {
            if (result == ShootResult.FAIL_CAMERA_DISCONNECTED) {
                logViewer.append("initiateCapture:FAIL_CAMERA_DISCONNECTED");
            } else if (result == ShootResult.FAIL_STORE_FULL) {
                logViewer.append("initiateCapture:FAIL_STORE_FULL");
            } else if (result == ShootResult.FAIL_DEVICE_BUSY) {
                logViewer.append("initiateCapture:FAIL_DEVICE_BUSY");
            } else if (result == ShootResult.SUCCESS) {
                logViewer.append("initiateCapture:SUCCESS");
            }
            //test2
        }

        private class CaptureListener extends PtpipEventListener {
            private int latestCapturedObjectHandle;
            private boolean objectAdd = false;

            @Override
            public void onObjectAdded(int objectHandle) {
                this.objectAdd = true;
                this.latestCapturedObjectHandle = objectHandle;
                appendLogView("objectAdd:ObjectHandle " + latestCapturedObjectHandle);
            }

            @Override
            public void onCaptureComplete(int transactionId) {
                appendLogView("CaptureComplete");
                if (objectAdd) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnShoot.setEnabled(true);
                            textCameraStatus.setText(R.string.text_camera_standby);
                            new GetThumbnailTask(latestCapturedObjectHandle).execute();
                        }
                    });
                }
            }
        }

    }

    private class GetThumbnailTask extends AsyncTask<Void, Void, Void> {

        private int objectHandle;

        public GetThumbnailTask(int objectHandle) {
            this.objectHandle = objectHandle;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), getResources().getString(R.string.theta_ip_address));
                PtpObject thumbnail = camera.getThumb(objectHandle);
                byte[] thumbnailImage = thumbnail.getDataObject();
                GLPhotoActivity.startActivity(ObjectListActivity.this, cameraIpAddress, objectHandle, thumbnailImage);
                return null;

            } catch (IOException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
            } catch (ThetaException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
            }
            return null;
        }
    }

    private SocketFactory getWifiSocketFactory() {
        SocketFactory socketFactory = SocketFactory.getDefault();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isGalaxyDevice()) {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            Network[] allNetwork = cm.getAllNetworks();
            for (Network network : allNetwork) {
                NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(network);
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    socketFactory = network.getSocketFactory();
                }
            }
        }
        return socketFactory;
    }

    private static final String BRAND_SAMSUNG = "samsung";
    private static final String MANUFACTURER_SAMSUNG = "samsung";

    private boolean isGalaxyDevice() {
        if ((Build.BRAND != null) && (Build.BRAND.toLowerCase(Locale.ENGLISH).contains(BRAND_SAMSUNG))) {
            return true;
        }
        if ((Build.MANUFACTURER != null) && (Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains(MANUFACTURER_SAMSUNG))) {
            return true;
        }
        return false;
    }

    /**
     * Created by Admin on 2/6/2016.
     */
    public class Constants {
        public static final String FIREBASE_URL = "https://nextweaverproject.firebaseio.com"; // Replace with your own URL
    }

    public class MyReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifi.getScanResults();
            names = new ArrayList<String>();
            camCount = 0;

            //Loopping List to find THETA wifi
            for (int w = 0; w < results.size(); w++) {
                if (results.get(w).SSID.contains("THETA")) {
                    names.add((results.get(w)).SSID);
                    camCount++;
                }
            }

            //Case for camCount
            if (camCount > 1) {
                alertDialogWifi();

            } else if (camCount == 1) {
                //Check THETA
                ssIdTHETA = names.get(0);
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }
                connectToSSID(ssIdTHETA, passTHETA);

            } else if (camCount == 0) {
                Toast.makeText(getApplicationContext(), "No Camera Available :(",
                        Toast.LENGTH_LONG).show();
            }

        }
    }

    public void alertDialogWifi() {
        final AlertDialog alertDialog = new AlertDialog.Builder(ObjectListActivity.this).create();
        LayoutInflater inflater = getLayoutInflater();
        final View convertView = (View) inflater.inflate(R.layout.custom, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle("Choose the camera.");


        lv1 = (ListView) convertView.findViewById(R.id.listView1);
        lv1.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_tv, names));

        lv1.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // WiFi selected

                ssIdTHETA = (String) ((TextView) view).getText();
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }

                Toast.makeText(getApplicationContext(), "" + ssIdTHETA + " " + passTHETA,
                        Toast.LENGTH_LONG).show();

                connectToSSID(ssIdTHETA, passTHETA);

                alertDialog.dismiss();
                isAlertDialogShown = false;
            }
        });
        if (!isAlertDialogShown) {
            alertDialog.show();
            isAlertDialogShown = true;
        }
        Log.v("WifiLOG", "OPEN DIALOG WIFI");

    }
    public void connectCamWifi() {

        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        //Enable Wifi
        if (!wifi.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Wifi is disabled..making it enabled",
                    Toast.LENGTH_LONG).show();
            Log.v("WifiLOG", "ENABLE WIFI");
            wifi.setWifiEnabled(true);
        }

        //Register receiver
        myReceiver = new MyReceiver();
        registerReceiver(myReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        wifi.startScan();

    }

    private void connectToSSID(String ssid, String password) {
        unregisterReceiver(myReceiver);

        Log.v("WifiLOG", "START CONNECTING TO THETA WIFI ID:" + ssid + " PASS:" + password);
        WifiConfiguration ezconf = new WifiConfiguration();
        ezconf.priority = 40;
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        ezconf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

        ezconf.SSID = "\"" + ssid + "\"";
        ezconf.preSharedKey = "\"".concat(password).concat("\"");

        WifiManager wfMgr = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int networkId = wfMgr.addNetwork(ezconf);
        if (networkId != -1) {
            wfMgr.enableNetwork(networkId, true);
        }
    }

    public void disconnectWifi() {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }

    }

    public void reconnectWifi() {

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        wifiManager.setWifiEnabled(true);

    }

    void startAnim() {
        findViewById(R.id.avloadingIndicatorView2).setVisibility(View.VISIBLE);
    }

    void stopAnim() {
        findViewById(R.id.avloadingIndicatorView2).setVisibility(View.GONE);
    }
}



