package com.theta360.sample;

import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theta360.sample.view.ObjectRow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UploadActivity extends FragmentActivity implements OnMapReadyCallback {
    private Button btnNext;
    private MyAdapter2 mAdapter;
    private GoogleMapOptions options;
    private ViewPager mPager;
    private GoogleMap mMap;
    static Context context;
    public static FragmentManager fragmentManager;
    SupportMapFragment mapFragment;
    MapFragment mMapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.up_fragment);
        context = getApplicationContext();

        btnNext = (Button)findViewById(R.id.buttonNext);
        mAdapter = new MyAdapter2(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        // This is required to avoid a black flash when the map is loaded.  The flash is due
        // to the use of a SurfaceView as the underlying view of the map.
        mPager.requestTransparentRegion(mPager);

       /* fm.beginTransaction().replace(R.id.map, supportMapFragment).commit();*/


    }

    @Override
    public void onMapReady(GoogleMap map) {
        LatLng sydney = new LatLng(-33.867, 151.206);

        map.setMyLocationEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        map.addMarker(new MarkerOptions()
                .title("Sydney")
                .snippet("The most populous city in Australia.")
                .position(sydney));
    }


    /** A simple fragment that displays a TextView. */
    public static class TextFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
            return inflater.inflate(R.layout.activity_upload, container, false);

        }

    }

    /** A simple FragmentPagerAdapter that returns two TextFragment and a SupportMapFragment. */
    public class MyAdapter2 extends FragmentPagerAdapter  {

        public MyAdapter2(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new TextFragment();
                case 1:
                    return SupportMapFragment.newInstance();

                default:
                    return null;
            }
        }



    }

       /* mUserId = mAuth.getCurrentUser().getUid().toString();
        itemsUrl ="https://nextweaverproject.firebaseio.com/users/" + mUserId  ;
        Log.v("RektUP","GG  long URL "+lnUrl);
        Log.v("RektUP","GG short URL "+shUrl);
        Log.v("RektUP","Item URL "+itemsUrl);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReferenceFromUrl(itemsUrl);
        final String putKeyScene = myRef.child("Scenes").child("Scene").push().getKey();
        myRef.child("Scenes").child("KeyScene").child(putKeyScene).setValue(putKeyScene);
        myRef.child("Scenes").child("Scene").child(putKeyScene).child("SceneName").setValue("Name");
        putKeyStr = new ArrayList<String>();
        /*//**************-------------------------TESTING UPLOAD HERE!------------------**************************
     storage = FirebaseStorage.getInstance();
     // Create a storage reference from our app
     storageRef = storage.getReferenceFromUrl("gs://nextweaverproject.appspot.com/TestPicTheta/" + mUserId);
     // --------------------------- CHOOSE FILE TO UPLOAD HERE ------------------------------------ //
     int obj1 = objectRows.size()  ;
     int obj2 = objectRows2.size();
     Log.v("UPLOADED TO","Up1         " + obj1 );
     Log.v("UPLOADED TO","up2         " + obj2 );
     for(int z =0;z < obj1 ;z++){
     final String putKeyPic = myRef.child("Scenes").child("Scene").child(putKeyScene).child("Thumb").push().getKey();
     putKeyStr.add(putKeyPic);
     if(z > 2){
     myRef.child("Scenes").child("KeyPic").child(putKeyStr.get(z)).setValue(putKeyStr.get(z));
     }

     final ObjectRow upload1 = objectRows.get(z);
     // set file upload destination
     Log.v("UPLOADED TO","Can get Obj1" );
     if(upload1.isPhoto()){
     StorageReference riversRef = storageRef.child("images/" + upload1.getFileName() + ".jpg");
     image = upload1.getThumbnail();
     uploadTask = riversRef.putBytes(image);

     final int finalZ2 = z;
     uploadTask.addOnFailureListener(new OnFailureListener() {
    @Override
    public void onFailure(@NonNull Exception exception) {
    // Handle unsuccessful uploads
    Log.v("UPLOADED TO","Fail" + i + ".JPG");
    }
    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
    @Override
    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
    Uri downloadUrl = taskSnapshot.getDownloadUrl();
    String dlUrl = downloadUrl.toString();
    Log.v("UPLOADED TO","Upload Photo 1  " + finalZ2 +". " +upload1.getFileName() );
    Log.v("UPLOADED TO","Upload Photo 1  " + finalZ2 +". " +upload1.getFileName() + "short url :  " + shortUr );
    Log.v("UPLOADED TO","Upload Photo 1  " + finalZ2 +". " +upload1.getFileName() + "long url :  " + dlUrl);
    Map<String, String> post3 = new HashMap<String, String>();
    post3.put("id", String.valueOf(finalZ2 - 2));
    post3.put("PicName",upload1.getFileName());
    post3.put("LongUrl",dlUrl);
    post3.put("Location","sumware" + String.valueOf(finalZ2));
    myRef.child("Scenes").child("Scene").child(putKeyScene).child("Thumb").child(putKeyPic).setValue(post3);
    myRef.child("Scenes").child("Scene").child(putKeyScene).child("SceneId").setValue(finalZ2);

    }
    });

     }else Log.v("UPLOADED TO","Not Photo OBj1   " + z + "." );
     }
     for(int z =0;z < obj1 ;z++){
     final ObjectRow upload2 = objectRows2.get(z);
     Log.v("UPLOADED TO","Can get Obj2" );
     if(upload2.isPhoto()){
     StorageReference riversRefReal = storageRef.child("RealPic/" + upload2.getFileName() + ".jpg");
     realImageTest = upload2.getThumbnail();
     uploadTask2 = riversRefReal.putBytes(realImageTest);
     final int finalZ = z;
     final int finalZ1 = z;
     final int finalZ2 = z;
     uploadTask2.addOnFailureListener(new OnFailureListener() {
    @Override
    public void onFailure(@NonNull Exception exception) {
    // Handle unsuccessful uploads
    Log.v("UPLOADED TO","Fail t2" + i + ".JPG");
    }
    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
    @Override
    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
    Uri downloadUrl = taskSnapshot.getDownloadUrl();
    String dlUrl = downloadUrl.toString();
    Log.v("UPLOADED TO","Upload Photo  2 " + finalZ1 +". " +upload2.getFileName() );
    Log.v("UPLOADED TO","Upload Photo  2 " + finalZ1 +". " +upload2.getFileName() + "short url :  " + shortUr );
    Log.v("UPLOADED TO","Upload Photo  2 " + finalZ1 +". " +upload2.getFileName() + "long url :  " + dlUrl);
    // set file upload destination
    Map<String, String> post4 = new HashMap<String, String>();
    post4.put("id", String.valueOf(finalZ1 -2));
    post4.put("PicName", upload2.getFileName());
    post4.put("LongUrl", dlUrl);
    post4.put("Location", "sumware" + String.valueOf(finalZ1));
    myRef.child("Scenes").child("Scene").child(putKeyScene).child("Real").child(putKeyStr.get(finalZ2)).setValue(post4);
    }
    });

     }else Log.v("UPLOADED TO","Not Photo OBj2   " + z + "." );
     }*/


}
