package com.theta360.sample;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.MapFragment;
import com.squareup.picasso.Picasso;
import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptp.service.PtpipEventListeningService;
import com.theta360.lib.ptpip.connector.PtpipConnector;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.SocketFactory;


public class NavigateActivity extends AppCompatActivity {
        //Defining Variables
        private Toolbar toolbar;
        private NavigationView navigationView;
    private NavigationView navigationView2;
        private DrawerLayout drawerLayout;
    private String personName ;
    private String personEmail ;
    private String personId ;
    private String personPhoto ;
    private ImageView profilePic;
    private TextView userEmail;
    private TextView userName;
    private TextView textView;
    private ArrayList<String> names;
    private MyReceiver myReceiver;
    private String ssIdTHETA;
    private String passTHETA;
    private WifiManager wifi;
    private List<ScanResult> results;
    private int camCount = 0;
    private ListView lv, lv1;
    private boolean isAlertDialogShown = false;
    private SurvayDetail detail;
    private boolean cameraStat=false;
    MyEventListener fragmentEventListener;
    private Location location;

    public SurvayDetail getSurDetail() {
        return surDetail;
    }

    public void setSurDetail(SurvayDetail surDetail) {
        this.surDetail = surDetail;
    }

    public static SurvayDetail surDetail;



        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            setContentView(R.layout.activity_navigate);
            // Initializing Toolbar and setting it as the actionbar
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            textView = (TextView)findViewById(R.id.textView4) ;
            //Initializing NavigationView
            if (canGetLocation() == true) {
                //DO SOMETHING USEFUL HERE. ALL GPS PROVIDERS ARE CURRENTLY ENABLED
                Log.v("check"," location is Enable.");
            } else {
                //SHOW OUR SETTINGS ALERT, AND LET THE USE TURN ON ALL THE GPS PROVIDERS
                Log.v("check"," location is Disable.");
                showSettingsAlert();
            }
            Intent intent = getIntent();
            personName = intent.getStringExtra("userName");
            personEmail = intent.getStringExtra("userEmail");
            personId = intent.getStringExtra("userId");
            personPhoto = intent.getStringExtra("userPic");
            Log.v("test",personPhoto);
            navigationView = (NavigationView) findViewById(R.id.navigation_view);
            View view = navigationView.getHeaderView(0);
            navigationView2 = (NavigationView) findViewById(R.id.nav_view2);
            View view2 = navigationView2.getHeaderView(0);
           detail = new SurvayDetail();
            userEmail = (TextView)view.findViewById(R.id.email) ;
            userName = (TextView)view.findViewById(R.id.username) ;
            profilePic = (ImageView)view.findViewById(R.id.profile_image) ;
            Picasso.with(this).load(personPhoto).into(profilePic);
            userEmail.setText(personEmail);
            userName.setText(personName);
            drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
            GoogleMapFragment myFragment = new GoogleMapFragment();
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

                // This method will trigger on item Click of navigation menu
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {


                    //Checking if the item is in checked state or not, if not make it in checked state
                    if(menuItem.isChecked()) menuItem.setChecked(false);
                    else menuItem.setChecked(true);

                    //Closing drawer on item click
                    drawerLayout.closeDrawers();
                    android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                    //Check to see which item was being clicked and perform appropriate action
                    switch (menuItem.getItemId()){
                        //Replacing the main content with ContentFragment Which is our Inbox View;
                        case R.id.camera:
                            //Toast.makeText(getApplicationContext(),"New Project",Toast.LENGTH_SHORT).show();
                            new CheckCameraStatus().execute();
                            textView.setVisibility(View.GONE);
                            if(cameraStat){
                                NavigationDrawerFragment fragment = new NavigationDrawerFragment();
                                fragmentTransaction.replace(R.id.frame,fragment);
                                fragmentTransaction.commit();
                            }else{
                                connectCamWifi();
                                Toast.makeText(getApplicationContext(),"Camera is not connected please try again.",Toast.LENGTH_SHORT).show();
                            }
                           /* textView.setVisibility(View.GONE);
                            NavigationDrawerFragment fragment = new NavigationDrawerFragment();
                            fragmentTransaction.replace(R.id.frame,fragment);
                            fragmentTransaction.commit();
*/
                            return true;

                        // For rest of the options we just show a toast on click
                        case R.id.c2t:
                            Toast.makeText(getApplicationContext(),"Connecting to Theta",Toast.LENGTH_SHORT).show();
                            connectCamWifi();
                            return true;
                        case R.id.c2w:
                            Toast.makeText(getApplicationContext(),"Connecting to Wifi",Toast.LENGTH_SHORT).show();
                            reconnectWifi();
                            return true;
                        case R.id.up2firebase:
                            //Toast.makeText(getApplicationContext(),"Upload to Firebase",Toast.LENGTH_SHORT).show();
                            textView.setVisibility(View.GONE);
                            ChooseLogoFragment uploadFrag = new ChooseLogoFragment();
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame,uploadFrag );
                            fragmentTransaction.commit();
                            return true;
                        case R.id.firebaseDl:
                            Toast.makeText(getApplicationContext(),"Download from Firebase",Toast.LENGTH_SHORT).show();
                            textView.setVisibility(View.GONE);
                           ViewFragment viewFrag = new ViewFragment();
                            fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.replace(R.id.frame,viewFrag);
                            fragmentTransaction.commit();
                            return true;
                        default:
                            Toast.makeText(getApplicationContext(),"Somethings Wong",Toast.LENGTH_SHORT).show();
                            return true;

                    }
                }
            });

            // Initializing Drawer Layout and ActionBarToggle
            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){

                @Override
                public void onDrawerClosed(View drawerView) {
                    // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                    super.onDrawerClosed(drawerView);
                }

                @Override
                public void onConfigurationChanged(Configuration newConfig) {
                    super.onConfigurationChanged(newConfig);
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                    super.onDrawerOpened(drawerView);
                }
            };

            //Setting the actionbarToggle to drawer layout
            drawerLayout.addDrawerListener(actionBarDrawerToggle);
            //calling sync state is necessay or else your hamburger icon wont show up
            actionBarDrawerToggle.syncState();






        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    public void disconnectWifi() {

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }

    }

    public void reconnectWifi() {

        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        wifiManager.setWifiEnabled(true);

    }
    public void connectCamWifi() {

        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        //Enable Wifi
        if (!wifi.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Wifi is disabled..making it enabled",
                    Toast.LENGTH_LONG).show();
            Log.v("WifiLOG", "ENABLE WIFI");
            wifi.setWifiEnabled(true);
        }

        //Register receiver
        myReceiver = new MyReceiver();
        registerReceiver(myReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        wifi.startScan();

    }
    private void connectToSSID(String ssid, String password) {
        unregisterReceiver(myReceiver);

        Log.v("WifiLOG", "START CONNECTING TO THETA WIFI ID:" + ssid + " PASS:" + password);
        WifiConfiguration ezconf = new WifiConfiguration();
        ezconf.priority = 40;
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        ezconf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

        ezconf.SSID = "\"" + ssid + "\"";
        ezconf.preSharedKey = "\"".concat(password).concat("\"");

        WifiManager wfMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int networkId = wfMgr.addNetwork(ezconf);
        if (networkId != -1) {
            wfMgr.enableNetwork(networkId, true);
        }
    }
    public class MyReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifi.getScanResults();
            names = new ArrayList<String>();
            camCount = 0;

            //Loopping List to find THETA wifi
            for (int w = 0; w < results.size(); w++) {
                if (results.get(w).SSID.contains("THETA")) {
                    names.add((results.get(w)).SSID);
                    camCount++;
                }
            }

            //Case for camCount
            if (camCount > 1) {
                alertDialogWifi();

            } else if (camCount == 1) {
                //Check THETA
                ssIdTHETA = names.get(0);
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }
                connectToSSID(ssIdTHETA, passTHETA);

            } else if (camCount == 0) {
                Toast.makeText(getApplicationContext(), "No Camera Available :(",
                        Toast.LENGTH_LONG).show();
            }

        }
    }
    public void alertDialogWifi() {
        final AlertDialog alertDialog = new AlertDialog.Builder(getApplication()).create();
        LayoutInflater inflater = getLayoutInflater();
        final View convertView = (View) inflater.inflate(R.layout.custom, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle("Choose the camera.");


        lv1 = (ListView) convertView.findViewById(R.id.listView1);
        lv1.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_tv, names));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // WiFi selected

                ssIdTHETA = (String) ((TextView) view).getText();
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }

                Toast.makeText(getApplicationContext(), "" + ssIdTHETA + " " + passTHETA,
                        Toast.LENGTH_LONG).show();

                connectToSSID(ssIdTHETA, passTHETA);

                alertDialog.dismiss();
                isAlertDialogShown = false;
            }
        });
        if (!isAlertDialogShown) {
            alertDialog.show();
            isAlertDialogShown = true;
        }
        Log.v("WifiLOG", "OPEN DIALOG WIFI");

    }
    public void showSettingsAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("Error!");
        // Setting Dialog Message
        alertDialog.setMessage("Please enable location service. ");
        // On pressing Settings button
        alertDialog.setPositiveButton(
                "Enable it",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                });
        //alertDialog.setNegativeButton("No", null);
        alertDialog.show();
    }

    public boolean canGetLocation() {
        boolean result = true;
        LocationManager lm = null;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        if (lm == null)

            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {

        }
       /* try {
            network_enabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }*/
        if (gps_enabled == false ) {
            result = false;
        } else {
            result = true;
        }

        return result;
    }
  public boolean camStat(){
      boolean stat = false;
      String cameraIpAddress = getResources().getString(R.string.theta_ip_address);

      try {
          PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);
          stat = true;
      } catch (IOException e) {
          e.printStackTrace();
      } catch (ThetaException e) {
          e.printStackTrace();
          stat = false;
      }



      Log.v("stat", "cam stat :" + stat );

      return  stat;
  }
    private SocketFactory getWifiSocketFactory() {
        SocketFactory socketFactory = SocketFactory.getDefault();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isGalaxyDevice()) {
            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            Network[] allNetwork = cm.getAllNetworks();
            for (Network network : allNetwork) {
                NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(network);
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    socketFactory = network.getSocketFactory();
                }
            }
        }
        return socketFactory;
    }
    private static final String BRAND_SAMSUNG = "samsung";
    private static final String MANUFACTURER_SAMSUNG = "samsung";
    private class CheckCameraStatus extends AsyncTask<Void, String, Boolean> {




        @Override
        protected Boolean  doInBackground(Void... voids) {
            boolean status = false;
            status = camStat();
            return status;
        }

        @Override
        protected void onPostExecute(Boolean stat) {
            super.onPostExecute(stat);
            if(stat){
                cameraStat = true;
            }else cameraStat = false;

        }
    }



    private boolean isGalaxyDevice() {
        if ((Build.BRAND != null) && (Build.BRAND.toLowerCase(Locale.ENGLISH).contains(BRAND_SAMSUNG))) {
            return true;
        }
        if ((Build.MANUFACTURER != null) && (Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains(MANUFACTURER_SAMSUNG))) {
            return true;
        }
        return false;
    }
}
