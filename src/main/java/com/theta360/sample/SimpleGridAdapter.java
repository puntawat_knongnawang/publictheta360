package com.theta360.sample;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theta360.sample.view.PersonData;

import java.util.ArrayList;

/**
 * Created by Admin on 20/6/2016.
 */
public class SimpleGridAdapter extends RecyclerView.Adapter<SimpleGridAdapter.MyViewHolder> {

    private ArrayList<Bitmap> bitmapsDataSet;
    public static class MyViewHolder extends RecyclerView.ViewHolder  {
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.logo_image);
        }


    }

    public SimpleGridAdapter(ArrayList<Bitmap> bitmaps) {
        this.bitmapsDataSet = bitmaps;
    }

    public ArrayList<Bitmap> getBitmapsDataSet() {
        return bitmapsDataSet;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.logos_layout, parent, false);

//        view.setOnClickListener(FromFirebaseActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {


        ImageView imageView = holder.imageViewIcon;
        if(!bitmapsDataSet.get(listPosition).equals(null)){
            imageView.setImageBitmap(bitmapsDataSet.get(listPosition));
        }else Log.v("null",listPosition + "  Nulled");

    }

    @Override
    public int getItemCount() {
        return bitmapsDataSet.size();
    }

}
