package com.theta360.sample;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.entity.DeviceInfo;
import com.theta360.lib.ptpip.entity.ObjectHandles;
import com.theta360.lib.ptpip.entity.ObjectInfo;
import com.theta360.lib.ptpip.entity.PtpObject;
import com.theta360.lib.ptpip.entity.StorageIds;
import com.theta360.lib.ptpip.entity.StorageInfo;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.net.SocketFactory;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class FromCameraFragment extends Fragment {
    private View loadingView;
    private String cameraIpAddress;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    List<ObjectRow> objectRows2;
    private ObjectHandles objectHandles;
    private ObjectRow objectRow;
    private ObjectRow objectRow2;
    private Button okBut;
    private ArrayList<Integer> pos;
    private String sceneName;
    private  String ownerName;
    private String survayerName;
    private String locationName;
    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_from_camera, container, false);

        loadingView = (View) v.findViewById(R.id.loading_camera);
        okBut = (Button)v.findViewById(R.id.buttonOk) ;
        pos = new ArrayList<>();

        cameraIpAddress = getResources().getString(R.string.theta_ip_address);
        objectRows2 = new ArrayList<ObjectRow>();
        objectRow2 = new ObjectRow();
        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list2);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        new LoadObjectListTask().execute();
okBut.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
       ProjectFragment toProject = new ProjectFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("objectRows2", (ArrayList<? extends Parcelable>) objectRows2);
        toProject.setArguments(bundle);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, toProject);
        transaction.addToBackStack(null);
        transaction.remove(FromCameraFragment.this).commit();

    }
});
        return v;
    }
    private class LoadObjectListTask extends AsyncTask<Void, String, List<ObjectRow>> {

        public LoadObjectListTask() {

        }

        @Override
        protected void onPreExecute() {
            startAnim();
        }

        @Override
        protected List<ObjectRow> doInBackground(Void... params) {
            try {
                publishProgress("------");
                publishProgress("connecting to " + cameraIpAddress + "...");
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);

                DeviceInfo deviceInfo = camera.getDeviceInfo();
                publishProgress(deviceInfo.getClass().getSimpleName() + ":<" + deviceInfo.getModel() + ", " + deviceInfo.getDeviceVersion() + ", " + deviceInfo.getSerialNumber() + ">");
                objectHandles = camera.getObjectHandles(PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT);
                int objectHandleCount = objectHandles.size();
                publishProgress("getObjectHandles() received " + objectHandleCount + " handles.");
                List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
                StorageIds storageIDs = camera.getStorageIDs();
                int storageCount = storageIDs.size();
                for (int i = 0; i < storageCount; i++) {
                    int storageId = storageIDs.getStorageId(i);
                    StorageInfo storage = camera.getStorageInfo(storageId);
                    ObjectRow storageCapacity = new ObjectRow();
                    int freeSpaceInImages = storage.getFreeSpaceInImages();
                    int megaByte = 1024 * 1024;
                    long freeSpace = storage.getFreeSpaceInBytes() / megaByte;
                    long maxSpace = storage.getMaxCapacity() / megaByte;
                    storageCapacity.setFileName("Free space: " + freeSpaceInImages + "[shots] (" + freeSpace + "/" + maxSpace + "[MB])");
                    objectRows.add(storageCapacity);

                }
                for (int i = 0; i < objectHandleCount; i++) {
                    objectRow = new ObjectRow();
                    final int objectHandle = objectHandles.getObjectHandle(i);
                    ObjectInfo object = camera.getObjectInfo(objectHandle);
                    objectRow.setObjectHandle(objectHandle);
                    objectRow.setFileName(object.getFilename());
                    objectRow.setCaptureDate(object.getCaptureDate());
                    if (object.getObjectFormat() == ObjectInfo.OBJECT_FORMAT_CODE_EXIF_JPEG) {
                        objectRow.setIsPhoto(true);
                        PtpObject thumbnail = camera.getThumb(objectHandle);
                        final byte[] thumbnailImage = thumbnail.getDataObject();
                        objectRow.setThumbnail(thumbnailImage);

                    } else {
                        objectRow.setIsPhoto(false);
                    }
                    objectRows.add(objectRow);
                }
                return objectRows;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {

            }
        }

        @Override
        protected void onPostExecute(final List<ObjectRow> objectRows) {
            if (objectRows != null) {
                //final ObjectListArrayAdapter objectListArrayAdapter = new ObjectListArrayAdapter(ObjectListActivity.this, R.layout.listlayout_object, objectRows);
                people = new ArrayList<>();
                Log.v("Err", String.valueOf(objectRows.size()));
                for (int i = 3; i < objectRows.size(); i++) {
                    byte[] test = objectRows.get(i).getThumbnail();
                    Bitmap thumbPic = BitmapFactory.decodeByteArray(test, 0, test.length);
                    String thumbName = objectRows.get(i).getFileName().toString();
                    Log.v("capdate : ",objectRows.get(i).getCaptureDate());
                    people.add(new PersonData(
                            thumbName,
                            thumbPic
                    ));
                }
                adapter = new MyAdapter(people);
                final AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(420);
                mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));
                ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        byte[] thumbnail = objectRows.get(position + 3).getThumbnail();
                        int objectHandle = objectRows.get(position + 3).getObjectHandle();
                        GLPhotoActivity.startActivity(getActivity(), cameraIpAddress, objectHandle, thumbnail);
                        // do it
                    }
                });
                ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                        objectRow2 = objectRows.get(position+3);
                        objectRows2.add(objectRow2);

                        pos.add(position);
                        removeAt(position);
                        return false;
                    }
                });

            }


            stopAnim();
        }

        @Override
        protected void onCancelled() {
            stopAnim();
        }
    }

    private SocketFactory getWifiSocketFactory() {
        SocketFactory socketFactory = SocketFactory.getDefault();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isGalaxyDevice()) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network[] allNetwork = cm.getAllNetworks();
            for (Network network : allNetwork) {
                NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(network);
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    socketFactory = network.getSocketFactory();
                }
            }
        }
        return socketFactory;
    }

    private static final String BRAND_SAMSUNG = "samsung";
    private static final String MANUFACTURER_SAMSUNG = "samsung";

    private boolean isGalaxyDevice() {
        if ((Build.BRAND != null) && (Build.BRAND.toLowerCase(Locale.ENGLISH).contains(BRAND_SAMSUNG))) {
            return true;
        }
        if ((Build.MANUFACTURER != null) && (Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains(MANUFACTURER_SAMSUNG))) {
            return true;
        }
        return false;
    }

    void startAnim() {
        loadingView.setVisibility(View.VISIBLE);
    }

    void stopAnim() {
        loadingView.setVisibility(View.GONE);
    }

    private class DeleteObjectTask extends AsyncTask<Void, Void, Void> {
private int selectDelete;

        public DeleteObjectTask(int selected) {
            selectDelete = selected;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                PtpipInitiator camera2 = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);
                camera2.deleteObject(selectDelete + 3,0);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ThetaException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            startAnim();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            stopAnim();
        }
    }
    public void removeAt(int position) {
        people.remove(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, people.size());
       // new DeleteObjectTask(position).execute();
    }

}