package com.theta360.sample;

/*
 * Copyright 2015 Google Inc. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.vr.sdk.widgets.common.VrWidgetView;
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener;
import com.google.vr.sdk.widgets.pano.VrPanoramaView;
import com.google.vr.sdk.widgets.pano.VrPanoramaView.Options;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import java.io.InputStream;

public class SimpleVrPanoramaActivity extends Activity {
    private static final String TAG = SimpleVrPanoramaActivity.class.getSimpleName();
    /** Configuration information for the panorama. **/
    private Options panoOptions = new Options();
    private ImageView imageView;
    private VrPanoramaView bmImage;
    private LruCache<String, Bitmap> mMemoryCache;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
        setContentView(R.layout.main_layout);
        //imageView = (ImageView)findViewById(R.id.imView);
        bmImage =(VrPanoramaView) findViewById(R.id.pano_view);
        Intent intent = getIntent();
        new DownloadImageTask(bmImage).execute(intent.getStringExtra("clickedItem"));
    }
    public Bitmap screenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        VrPanoramaView bmImage;
        private final ProgressDialog dialog = new ProgressDialog(SimpleVrPanoramaActivity.this);
        public DownloadImageTask(VrPanoramaView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
                in.close();
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            addBitmapToMemoryCache("cacheBit",mIcon11);
            return mIcon11;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.setMessage("Processing...");
            this.dialog.show();
        }
        protected void onPostExecute(Bitmap result) {
            this.dialog.dismiss();
            panoOptions = new Options();
            panoOptions.inputType = Options.TYPE_MONO;
            bmImage.loadImageFromBitmap(result,panoOptions);

           /* View v1 = getWindow().peekDecorView().getRootView();
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap =  v1.getDrawingCache(true);
            imageView.setImageBitmap(bitmap);*/



        }
    }
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
       Bitmap result = getBitmapFromMemCache("cacheBit");
        panoOptions = new Options();
        panoOptions.inputType = Options.TYPE_MONO;
        bmImage.loadImageFromBitmap(result,panoOptions);
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
