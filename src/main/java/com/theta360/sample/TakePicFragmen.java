package com.theta360.sample;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.CircleProgress;
import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ThetaException;
import com.theta360.lib.ptpip.entity.PtpObject;
import com.theta360.lib.ptpip.entity.Response;
import com.theta360.lib.ptpip.eventlistener.PtpipEventListener;
import com.theta360.lib.ptpip.settingvalue.ISOSpeed;
import com.theta360.lib.ptpip.settingvalue.ShutterSpeed;

import java.io.IOException;
import java.util.Locale;

import javax.net.SocketFactory;

import static com.google.vr.cardboard.ThreadUtils.runOnUiThread;


/**
 * A simple {@link Fragment} subclass.
 */
public class TakePicFragmen extends Fragment {
private Button buttonShoot;
    private  SeekBar seekBarISO;
    private SeekBar seekBarShutSP;
    private TextView isoVal;
    private TextView shutVal;
    private Switch shutSound;
    private boolean volu;
    private ShutterSpeed posShut;
    private ISOSpeed posIso;
    private String cameraIpAddress;
    private  TextView textCameraStatus;
    private CircleProgress circleProgress;
    private Button backBut;
    public TakePicFragmen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_take_pic, container, false);

        buttonShoot = (Button)v.findViewById(R.id.btn_shoot);
        shutSound = (Switch) v.findViewById(R.id.switchShSnd);
        seekBarISO = (SeekBar) v.findViewById(R.id.seekBarISO);
        backBut = (Button)v.findViewById(R.id.buttonBackToProj2) ;
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        circleProgress = (CircleProgress) v.findViewById(R.id.circle_progress) ;
        seekBarShutSP = (SeekBar) v.findViewById(R.id.seekBarShutSP);
        isoVal = (TextView) v.findViewById(R.id.textViewISOValue);
        shutVal = (TextView) v.findViewById(R.id.textViewShutSPVal);
        textCameraStatus = (TextView) v.findViewById(R.id.camera_status);
        cameraIpAddress = getResources().getString(R.string.theta_ip_address);
        new ConnectCam().execute();
        posIso = ISOSpeed.AUTO;
        posShut = ShutterSpeed.AUTO;
        volu = false;
        shutSound.setTextOn("On");
        shutSound.setTextOff("Off");
        shutSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    volu = true;
                } else {
                    volu = false;
                }
            }
        });
        seekBarISO.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int pos, boolean b) {
                if (seekBar.equals(seekBarISO)) {
                    switch (pos) {
                        case 0:
                            posIso = ISOSpeed.AUTO;
                            isoVal.setText("AUTO");
                            break;
                        case 1:
                            posIso = ISOSpeed.ISO100;
                            isoVal.setText("100");
                            break;
                        case 2:
                            posIso = ISOSpeed.ISO125;
                            isoVal.setText("125");
                            break;
                        case 3:
                            posIso = ISOSpeed.ISO160;
                            isoVal.setText("160");
                            break;
                        case 4:
                            posIso = ISOSpeed.ISO200;
                            isoVal.setText("200");
                            break;
                        case 5:
                            posIso = ISOSpeed.ISO250;
                            isoVal.setText("250");
                            break;
                        case 6:
                            posIso = ISOSpeed.ISO320;
                            isoVal.setText("320");
                            break;
                        case 7:
                            posIso = ISOSpeed.ISO400;
                            isoVal.setText("400");
                            break;
                        case 8:
                            posIso = ISOSpeed.ISO500;
                            isoVal.setText("500");
                            break;
                        case 9:
                            posIso = ISOSpeed.ISO640;
                            isoVal.setText("640");
                            break;
                        case 10:
                            posIso = ISOSpeed.ISO800;
                            isoVal.setText("800");
                            break;
                        case 11:
                            posIso = ISOSpeed.ISO1000;
                            isoVal.setText("1000");
                            break;
                        case 12:
                            posIso = ISOSpeed.ISO1250;
                            isoVal.setText("1250");
                            break;
                        case 13:
                            posIso = ISOSpeed.ISO1600;
                            isoVal.setText("1600");
                            break;
                        default:
                            posIso = ISOSpeed.AUTO;
                            isoVal.setText("DEFAULT");
                            break;
                    }
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        seekBarShutSP.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int pos, boolean b) {
                if (seekBar.equals(seekBarShutSP)) {
                    switch (pos) {
                        case 0:
                            posShut = ShutterSpeed.AUTO;
                            shutVal.setText("AUTO");
                            break;
                        case 1:
                            posShut = ShutterSpeed.SPEED1_8000;
                            shutVal.setText("1/8000");
                            break;
                        case 2:
                            posShut = ShutterSpeed.SPEED1_6400;
                            shutVal.setText("1/6400");
                            break;
                        case 3:
                            posShut = ShutterSpeed.SPEED1_2000;
                            shutVal.setText("1/2000");
                            break;
                        case 4:
                            posShut = ShutterSpeed.SPEED1_1000;
                            shutVal.setText("1/1000");
                            break;
                        case 5:
                            posShut = ShutterSpeed.SPEED1_500;
                            shutVal.setText("1/500");
                            break;
                        case 6:
                            posShut = ShutterSpeed.SPEED1_250;
                            shutVal.setText("1/250");
                            break;
                        case 7:
                            posShut = ShutterSpeed.SPEED1_125;
                            shutVal.setText("1/125");
                            break;
                        case 8:
                            posShut = ShutterSpeed.SPEED1_50;
                            shutVal.setText("1/50");
                            break;
                        case 9:
                            posShut = ShutterSpeed.SPEED1_25;
                            shutVal.setText("1/25");
                            break;
                        case 10:
                            posShut = ShutterSpeed.SPEED1_10;
                            shutVal.setText("1/10");
                            break;
                        default:
                            posShut = ShutterSpeed.AUTO;
                            shutVal.setText("DEFAULT");
                            break;

                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        buttonShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonShoot.setEnabled(false);
                textCameraStatus.setText(R.string.text_camera_synthesizing);
                new ShootTask(posIso, posShut, volu).execute();

            }
        });
        return v;
    }
    private static enum ShootResult {
        SUCCESS, FAIL_CAMERA_DISCONNECTED, FAIL_STORE_FULL, FAIL_DEVICE_BUSY
    }

    private class ShootTask extends AsyncTask<Void, Void, ShootResult> {
        private ISOSpeed isoPos;
        private ShutterSpeed shutSpPos;
        private boolean vol;


        public ShootTask(ISOSpeed isoPosition, ShutterSpeed shutSpPosition, boolean volume) {
            isoPos = isoPosition;
            shutSpPos = shutSpPosition;
            vol = volume;
        }

        @Override
        protected void onPreExecute() {
            //logViewer.append("initiateCapture");
        }

        ;

        @Override
        protected ShootResult doInBackground(Void... params) {
            CaptureListener postviewListener = new CaptureListener();
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), getResources().getString(R.string.theta_ip_address));
                if (vol) {
                    camera.setAudioVolume(50);
                } else {
                    camera.setAudioVolume(0);
                }


                camera.setShutterSpeed(shutSpPos);
                camera.setExposureIndex(isoPos);
                camera.initiateCapture(postviewListener);
                return ShootResult.SUCCESS;

            } catch (IOException e) {
                return ShootResult.FAIL_CAMERA_DISCONNECTED;
            } catch (ThetaException e) {
                if (Response.RESPONSE_CODE_STORE_FULL == e.getStatus()) {
                    return ShootResult.FAIL_STORE_FULL;
                } else if (Response.RESPONSE_CODE_DEVICE_BUSY == e.getStatus()) {
                    return ShootResult.FAIL_DEVICE_BUSY;
                } else {
                    return ShootResult.FAIL_CAMERA_DISCONNECTED;
                }
            }
        }

        @Override
        protected void onPostExecute(ShootResult result) {
            if (result == ShootResult.FAIL_CAMERA_DISCONNECTED) {
                //logViewer.append("initiateCapture:FAIL_CAMERA_DISCONNECTED");
            } else if (result == ShootResult.FAIL_STORE_FULL) {
                //logViewer.append("initiateCapture:FAIL_STORE_FULL");
            } else if (result == ShootResult.FAIL_DEVICE_BUSY) {
               // logViewer.append("initiateCapture:FAIL_DEVICE_BUSY");
            } else if (result == ShootResult.SUCCESS) {
                //logViewer.append("initiateCapture:SUCCESS");
            }
            //test2
        }

        private class CaptureListener extends PtpipEventListener {
            private int latestCapturedObjectHandle;
            private boolean objectAdd = false;

            @Override
            public void onObjectAdded(int objectHandle) {
                this.objectAdd = true;
                this.latestCapturedObjectHandle = objectHandle;
                //appendLogView("objectAdd:ObjectHandle " + latestCapturedObjectHandle);
            }

            @Override
            public void onCaptureComplete(int transactionId) {
                //appendLogView("CaptureComplete");
                if (objectAdd) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            buttonShoot.setEnabled(true);
                            textCameraStatus.setText(R.string.text_camera_standby);
                            new GetThumbnailTask(latestCapturedObjectHandle).execute();
                        }
                    });
                }
            }
        }

    }
    private class GetThumbnailTask extends AsyncTask<Void, Void, Void> {

        private int objectHandle;

        public GetThumbnailTask(int objectHandle) {
            this.objectHandle = objectHandle;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), getResources().getString(R.string.theta_ip_address));
                PtpObject thumbnail = camera.getThumb(objectHandle);
                byte[] thumbnailImage = thumbnail.getDataObject();
                GLPhotoActivity.startActivity(getActivity(), cameraIpAddress, objectHandle, thumbnailImage);
                return null;

            } catch (IOException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
            } catch (ThetaException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
            }
            return null;
        }
    }
    private SocketFactory getWifiSocketFactory() {
        SocketFactory socketFactory = SocketFactory.getDefault();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isGalaxyDevice()) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network[] allNetwork = cm.getAllNetworks();
            for (Network network : allNetwork) {
                NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(network);
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    socketFactory = network.getSocketFactory();
                }
            }
        }
        return socketFactory;
    }
    private static final String BRAND_SAMSUNG = "samsung";
    private static final String MANUFACTURER_SAMSUNG = "samsung";

    private boolean isGalaxyDevice() {
        if ((Build.BRAND != null) && (Build.BRAND.toLowerCase(Locale.ENGLISH).contains(BRAND_SAMSUNG))) {
            return true;
        }
        if ((Build.MANUFACTURER != null) && (Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains(MANUFACTURER_SAMSUNG))) {
            return true;
        }
        return false;
    }
    private void changeCameraStatus(final String resid) {
        runOnUiThread(new Runnable() {
            public void run() {
                textCameraStatus.setText(resid);
            }
        });
    }
    private class ConnectCam extends AsyncTask<Object, Object, Integer> {

        int batteryLevel;

        public ConnectCam() {

        }

        @Override
        protected Integer doInBackground(Object... objects) {
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), getResources().getString(R.string.theta_ip_address));

                batteryLevel =  camera.getBatteryLevel().getValue();
                Log.v("battery","bat = "+ batteryLevel);
                changeCameraStatus("Stand By");

            } catch (IOException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
            } catch (ThetaException e) {
                String errorLog = Log.getStackTraceString(e);
                //logViewer.append(errorLog);
                changeCameraStatus("Not Connect");
            }
            return batteryLevel;
        }

        @Override
        protected void onPreExecute() {

            super.onPreExecute();
            //circleProgress.setProgress(100);

        }

        @Override
        protected void onPostExecute(Integer progress) {
            super.onPostExecute(progress);
            circleProgress.setProgress(progress);
        }


    }
}
