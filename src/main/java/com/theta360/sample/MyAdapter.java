package com.theta360.sample;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.theta360.sample.view.PersonData;

import java.util.ArrayList;

/**
 * Created by Admin on 20/6/2016.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private ArrayList<PersonData> peopleDataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        TextView textViewName;
        TextView textViewEmail;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.card_text);
//            this.textViewEmail = (TextView) itemView.findViewById(R.id.textViewEmail);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.card_image);
        }

        @Override
        public boolean onLongClick(View v) {
            v.setBackgroundColor(Color.CYAN);

            return false;
        }
    }

    public MyAdapter(ArrayList<PersonData> people) {
        this.peopleDataSet = people;
    }

    public ArrayList<PersonData> getPeopleDataSet() {
        return peopleDataSet;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_layout, parent, false);

//        view.setOnClickListener(FromFirebaseActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
//        TextView textViewEmail = holder.textViewEmail;
        ImageView imageView = holder.imageViewIcon;
        if(!peopleDataSet.get(listPosition).getImage().equals(null)){
            textViewName.setText(peopleDataSet.get(listPosition).getName());

            imageView.setImageBitmap(peopleDataSet.get(listPosition).getImage());
        }else Log.v("null",listPosition + "  Nulled");

    }

    @Override
    public int getItemCount() {
        return peopleDataSet.size();
    }

}

