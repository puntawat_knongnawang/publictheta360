package com.theta360.sample;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class FirebaseFragment extends Fragment {
    private LruCache<String, Bitmap> mMemoryCache;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    private Bitmap thumbPic;
    private String thumbName;
    private ArrayList<String> listReal;
    private View loadingView;
    private List<String> thumbUrl;
    private List<String> realUrl;
    private String fUID;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase mDatabase;
    private String itemsUrl;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_firebase, container, false);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
        firebaseAuth = FirebaseAuth.getInstance();
        fUID = firebaseAuth.getCurrentUser().getUid();
        itemsUrl = "https://nextweaverproject.firebaseio.com/users/" + fUID;
        mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = mDatabase.getReferenceFromUrl(itemsUrl);
        thumbUrl = new ArrayList<>();
        realUrl = new ArrayList<>();
        myRef.child("Scenes").child("Scene").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot children : dataSnapshot.getChildren()) {
                    for (DataSnapshot child : children.getChildren()) {
                        //Log.v("key1","   " + child.getKey());
                        if (child.getKey().equals("Thumb")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                            thumbUrl.add(temp);
                                        }

                                    }
                                }
                            }
                        }
                        if (child.getKey().equals("Real")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4R", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                            realUrl.add(temp);
                                        }

                                    }
                                }
                            }
                        }
                    }


                    // Log.v("key2","   " + thumbUrl);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        if (thumbUrl != null) {
            Log.v("keyResult", "   " + thumbUrl + "  " + thumbUrl.size());
        } else Log.v("keyResult", "   " + thumbUrl + "  ");
        /*final Intent intent = getIntent();
        ArrayList<String> list =
                (ArrayList<String>)intent.getSerializableExtra("thumbUrl");
        listReal =
                (ArrayList<String>)intent.getSerializableExtra("realUrl");
        Log.v("key","   " + list);
        new GetThumbnailTask(list).execute();*/
        loadingView = (View) v.findViewById(R.id.loading_firebase);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        thumbName = "WOW such Image";
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
          /*    for(int i= 0; i < position;i++ ){
                  Intent intent1 = new Intent(getApplicationContext(),GridPicActivity.class);
                  intent1.putExtra("inform",new ArrayList<PersonData>(people));
                  startActivity(intent1);

                }*/
                /*Intent intent1 = new Intent(getContext(),SimpleVrPanoramaActivity.class);
                intent1.putExtra("clickedItem",listReal.get(position).toString());
                startActivity(intent1);*/

                // do it
            }
        });
        return v;
    }
    private class GetThumbnailTask extends AsyncTask<Void, Void, List<Bitmap> > {
        ArrayList<String> thumb;
        public GetThumbnailTask() {

        }

        public GetThumbnailTask(ArrayList<String> list) {
            thumb = list;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
            ObjectRow object = new ObjectRow();
            final List<String> urlStr = new ArrayList<>();
            Bitmap temp;
            List<Bitmap> bitmap = new ArrayList<>();
            for(int i=0;i < thumb.size();i++ ){
                temp = null;
                temp = getBitmapFromURL(thumb.get(i));
                if(temp == null){
                    temp = getBitmapFromURL(thumb.get(i));
                }else {
                    bitmap.add(temp);

                }
            }
            return bitmap;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            startAnim();
        }
        @Override
        protected void onPostExecute(List<Bitmap> bitmap) {
            super.onPostExecute(bitmap);
            // progressBar.setVisibility(View.GONE);
            people = new ArrayList<>();
            stopAnim();
            for(int i=0;i < bitmap.size();i++) {

                thumbPic = bitmap.get(i);
                addBitmapToMemoryCache(String.valueOf(i), bitmap.get(i));
                thumbName = i + " PIC ";
                people.add(new PersonData(
                        thumbName,
                        thumbPic
                ));

            }
            adapter = new MyAdapter(people);
            AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
            alphaAdapter.setDuration(500);
            mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));

            Log.v("key","Sumtin wong  ggggg");



            // byte[] thumbnailImage = thumbnail.getDataObject();

        }
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;
        } catch (IOException e) {
            // Log exception

            return null;
        }
    }
    void startAnim(){
       loadingView.setVisibility(View.VISIBLE);
    }

    void stopAnim(){
        loadingView.setVisibility(View.GONE);
    }
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
    public int getBitmapFromMemCacheNum() {
        return mMemoryCache.size();
    }

}
