package com.theta360.sample;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;


public class ChooseLogoFragment extends Fragment {
    private View loading;
    private RecyclerView recyclerView;
    private String fUID;
    private FirebaseAuth firebaseAuth;
    private FirebaseStorage mDatabase;
    private String itemsUrl;
    private ArrayList<String> urls;
    private ArrayList<String> picName;
    private Handler handler = new Handler();
    private String urlPic;
    private String sceneName;
    private  String ownerName;
    private String survayerName;
    private String locationName;
    private String date;
    private ArrayList<ObjectRow> objectThumb;
    private ArrayList<ObjectRow> objectReal;
    private Button finBut;
    private FirebaseDatabase fbDb;
    private DatabaseReference myref;
    private ArrayList<String> putKeyStr;
    private byte[] image;
    private byte[] realImageTest;
    private StorageReference myRef;
    private UploadTask uploadTask;
    private UploadTask uploadTask2;
    private ArrayList<LatLng> latLngs;
    private Button backBut;
private SimpleGridAdapter adapter;
    public ChooseLogoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_choose_logo, container, false);
        loading = (View)v.findViewById(R.id.loading_logos);
        backBut = (Button)v.findViewById(R.id.buttonBackToGGmap) ;
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        recyclerView = (RecyclerView)v.findViewById(R.id.object_list_logo);
        finBut = (Button)v.findViewById(R.id.buttonOkFinish) ;
        mDatabase = FirebaseStorage.getInstance();
        fbDb = FirebaseDatabase.getInstance();
        latLngs = new ArrayList<>();
        picName = new ArrayList<>();
        picName.add("albtmb641.png");
        picName.add("G_circle_logo.png");
        picName.add("jodioil_circle_logo.png");
        picName.add("mrcscrmn_circle_logo.png");
        picName.add("overwatch.png");
        if(getArguments() != null){
            Bundle bundle = getArguments();
            objectThumb = bundle.getParcelableArrayList("objectThumb");
            objectReal = bundle.getParcelableArrayList("objectReal");
            sceneName = bundle.getString("sceneName");
            ownerName = bundle.getString("ownerName");
            survayerName = bundle.getString("survayerName");
            locationName =  bundle.getString("locationName");
            date = bundle.getString("date");
            latLngs = bundle.getParcelableArrayList("latln");
            Log.v("input3","Scene name : " + sceneName);
            Log.v("input3","Owner name : " + ownerName);
            Log.v("input3","Survay name : " + survayerName);
            Log.v("input3","Location name : " + locationName);
            Log.v("input3","Date : "+date);
        }else{
            Toast.makeText(getContext(),"IT NULL MF",Toast.LENGTH_SHORT);
        }
        firebaseAuth = FirebaseAuth.getInstance();
        fUID = firebaseAuth.getCurrentUser().getUid();
        Log.v("storage", "Uid : " + fUID);
        itemsUrl = "gs://nextweaverproject.appspot.com";

        urls = new ArrayList<>();
        myRef = mDatabase.getReferenceFromUrl(itemsUrl);
for(int i = 0;i<picName.size();i++){
    myRef.child("TestPicTheta").child("logos").child(picName.get(i)).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
        @Override
        public void onSuccess(Uri uri) {
            Log.v("storage", "Url : " + uri.toString());
            urls.add(uri.toString());

        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            Log.v("storage", "Rekt");
        }
    });

}

        handler.postDelayed(runnable, 2000);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {

            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                urlPic = urls.get(position);
                Log.v("storage"," sendUrl : "+ urlPic);
                finBut.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String itemsUrl2 = "https://nextweaverproject.firebaseio.com/";

                        myref = fbDb.getReferenceFromUrl(itemsUrl2);
                        final String putKeyScene = myref.child("Scenes").push().getKey();
                        myref.child("Scenes").child(putKeyScene).child("SceneName").setValue(sceneName);
                        myref.child("KeyScene").child(sceneName).setValue(putKeyScene);
                        putKeyStr = new ArrayList<String>();
                        mDatabase = FirebaseStorage.getInstance();
                        // Create a storage reference from our app
                        myRef =  mDatabase.getReferenceFromUrl("gs://nextweaverproject.appspot.com/TestPicTheta/");
                        // --------------------------- CHOOSE FILE TO UPLOAD HERE ------------------------------------ //
                        int obj1 = objectThumb.size();
                        int obj2 = objectReal.size();
                        Log.v("UPLOADED TO", "Up1         " + obj1);
                        Log.v("UPLOADED TO", "up2         " + obj2);
                        for (int z = 0; z < obj1; z++) {
                            final String putKeyPic = myref.child("Scenes").child(putKeyScene).child("Thumb").push().getKey();
                            putKeyStr.add(putKeyPic);
                            myref.child("KeyPic").child(sceneName +"_" + objectReal.get(z).getFileName().substring(0,8)).setValue(putKeyStr.get(z));
                            final ObjectRow upload1 = objectThumb.get(z);
                            // set file upload destination
                            Log.v("UPLOADED TO", "Can get Obj1");
                                StorageReference riversRef = myRef.child("images/" + upload1.getFileName() + ".jpg");
                                 image = upload1.getThumbnail();
                                uploadTask = riversRef.putBytes(image);
                                final int finalZ2 = z;
                                uploadTask.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        String dlUrl = downloadUrl.toString();
                                        Log.v("UPLOADED TO", "Upload Photo 1  " + finalZ2 + ". " + upload1.getFileName());
                                        Log.v("UPLOADED TO", "Upload Photo 1  " + finalZ2 + ". " + upload1.getFileName() + "long url :  " + dlUrl);
                                        Map<String, String> post3 = new HashMap<String, String>();
                                        post3.put("id", String.valueOf(finalZ2 + 1));
                                        post3.put("PicName", upload1.getFileName());
                                        post3.put("LongUrl", dlUrl);
                                        post3.put("Location", "sumware" + String.valueOf(finalZ2));
                                        post3.put("Latitude", String.valueOf(latLngs.get(finalZ2).latitude));
                                        post3.put("Longitude", String.valueOf(latLngs.get(finalZ2).longitude));
                                        post3.put("Logo",urlPic);
                                        myref.child("Scenes").child(putKeyScene).child("Thumb").child(putKeyPic).setValue(post3);
                                        myref.child("Scenes").child(putKeyScene).child("SceneId").setValue(finalZ2);

                                    }
                                });

                        }
                        for (int z = 0; z < obj1; z++) {
                            final ObjectRow upload2 = objectReal.get(z);
                            Log.v("UPLOADED TO", "Can get Obj2");

                                StorageReference riversRefReal = myRef.child("RealPic/" + upload2.getFileName() + ".jpg");
                                realImageTest = upload2.getThumbnail();
                                uploadTask2 = riversRefReal.putBytes(realImageTest);
                                final int finalZ = z;
                                final int finalZ1 = z;
                                final int finalZ2 = z;
                                final int finalZ3 = z;
                                uploadTask2.addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception exception) {
                                        // Handle unsuccessful uploads
                                    }
                                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                                        String dlUrl = downloadUrl.toString();
                                        Log.v("UPLOADED TO", "Upload Photo  2 " + finalZ1 + ". " + upload2.getFileName());
                                        Log.v("UPLOADED TO", "Upload Photo  2 " + finalZ1 + ". " + upload2.getFileName() + "long url :  " + dlUrl);
                                        // set file upload destination
                                        Map<String, String> post4 = new HashMap<String, String>();
                                        post4.put("id", String.valueOf(finalZ1 + 1));
                                        post4.put("PicName", upload2.getFileName());
                                        post4.put("LongUrl", dlUrl);
                                        post4.put("Location", locationName);
                                        post4.put("Latitude", String.valueOf(latLngs.get(finalZ3).latitude));
                                        post4.put("Longitude", String.valueOf(latLngs.get(finalZ3).longitude));
                                        post4.put("Logo",urlPic);
                                        myref.child("Scenes").child(putKeyScene).child("Real").child(putKeyStr.get(finalZ2)).setValue(post4);
                                    }
                                });

                        }
                    }
                });
            }
        });


        return v;
    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            Log.v("storage", urls.toString());
            new GetThumbnailTask(urls).execute();
      /* and here comes the "trick" */
            handler.postDelayed(this, 2000);
            if(urls ==null){
                Log.v("storage", urls.toString());
                new GetThumbnailTask(urls).execute();
            }

            handler.removeCallbacks(runnable);

        }
    };
    private class GetThumbnailTask extends AsyncTask<Void, Void, List<Bitmap>> {
        ArrayList<String> thumb;

        public GetThumbnailTask() {

        }

        public GetThumbnailTask(ArrayList<String> list) {
            thumb = list;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
            ObjectRow object = new ObjectRow();
            final List<String> urlStr = new ArrayList<>();
            Bitmap temp;
            List<Bitmap> bitmap = new ArrayList<>();
            for (int i = 0; i < thumb.size(); i++) {
                temp = null;
                temp = getBitmapFromURL(thumb.get(i));
                if (temp == null) {
                    temp = getBitmapFromURL(thumb.get(i));
                } else {
                    bitmap.add(temp);

                }
            }
            return bitmap;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            startAnim();
        }

        @Override
        protected void onPostExecute(List<Bitmap> bitmap) {
            super.onPostExecute(bitmap);
            stopAnim();
            for (int i = 0; i < bitmap.size(); i++) {
                adapter = new SimpleGridAdapter((ArrayList<Bitmap>) bitmap);
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(300);
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
                recyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));
                // byte[] thumbnailImage = thumbnail.getDataObject();
            }

        }

        public Bitmap getBitmapFromURL(String src) {
            try {
                URL url = new URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                return myBitmap;
            } catch (IOException e) {
                // Log exception

                return null;
            }
        }

        void startAnim() {
           loading.setVisibility(View.VISIBLE);
        }

        void stopAnim() {
            loading.setVisibility(View.GONE);
        }
    }
}