package com.theta360.sample;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

class ViewHolder extends RecyclerView.ViewHolder{

    public ImageView imgThumbnail;
    public TextView tvNature;
    public TextView tvDesNature;

    public ViewHolder(View itemView) {
        super(itemView);
        imgThumbnail = (ImageView)itemView.findViewById(R.id.card_image);
        tvNature = (TextView)itemView.findViewById(R.id.card_text);
        //tvDesNature = (TextView)itemView.findViewById(R.id.tv_des_nature);
        /*itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                v.getContext().startActivity(new Intent(v.getContext(),FullInfo.class));

            }
        });*/
    }
}