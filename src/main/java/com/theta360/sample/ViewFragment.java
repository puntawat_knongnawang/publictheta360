package com.theta360.sample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class ViewFragment extends Fragment {
    private ArrayList<String> thumbUrl;
    private ArrayList<String> realUrl;
    private String fUID;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase mDatabase;
    private  String itemsUrl;
    private ArrayList<String> sceneName;
    private ArrayList<String> url;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    private Bitmap thumbPic;
    private String thumbName;
    private ArrayList<String> listReal;
    private View loadingView;
    private Handler handler = new Handler();
    private String sceneTemp;
    public ViewFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view, container, false);
        firebaseAuth = FirebaseAuth.getInstance();
        fUID = firebaseAuth.getCurrentUser().getUid();
        loadingView = (View)v.findViewById(R.id.loading_view);
        mRecyclerView = (RecyclerView)v.findViewById(R.id.object_list_view) ;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        itemsUrl = "https://nextweaverproject.firebaseio.com" ;
        mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = mDatabase.getReferenceFromUrl(itemsUrl);
        thumbUrl = new ArrayList<>();
        realUrl = new ArrayList<>();
        sceneName = new ArrayList<>();
        myRef.child("Scenes").addListenerForSingleValueEvent(new ValueEventListener() {
            int i = 0;
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot children : dataSnapshot.getChildren()) {
                    Log.v("key","   " + children.getKey());
                    for (DataSnapshot child : children.getChildren()) {
                        url = new ArrayList<String>();
                        Log.v("key1","   " + child.getKey());
                        if(child.getKey().equals("SceneName")){
                            sceneTemp = child.getValue().toString();
                            if(sceneTemp != null){
                                sceneName.add(sceneTemp);
                            }
                            Log.v("keyScene","   " + sceneName);

                        }

                        if (child.getKey().equals("Thumb")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                           // thumbUrl.add(temp);
                                           url.add(temp);

                                        }

                                    }
                                }
                            }
                        }
                        if(url.size() >0){
                            thumbUrl.add(url.get(0));
                        }

                       /* if (child.getKey().equals("Real")) {
                            for (DataSnapshot child2 : child.getChildren()) {
                                //Log.v("key2","   " + child2.getValue(String.class));
                                for (DataSnapshot child3 : child2.getChildren()) {
                                    //Log.v("key3","   " + child3.getKey());
                                    if (child3.getKey().equals("LongUrl")) {
                                        Log.v("key4R", "   " + child3.getValue(String.class));
                                        String temp = null;
                                        temp = child3.getValue(String.class);
                                        if (temp != null) {
                                            realUrl.add(temp);
                                        }

                                    }
                                }
                            }
                        }*/


                    }


                     Log.v("keyThumb","   " + thumbUrl.size());
                }
                handler.postDelayed(runnable, 2000);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        return v;


    }
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            Log.v("storage", thumbUrl.toString());
            new GetThumbnailTask(thumbUrl).execute();
      /* and here comes the "trick" */
           // handler.postDelayed(this, 2000);
            if(thumbUrl ==null){
                Log.v("storage", thumbUrl.toString());
                new GetThumbnailTask(thumbUrl).execute();
            }

            handler.removeCallbacks(runnable);

        }
    };
    private class GetThumbnailTask extends AsyncTask<Void, Void, List<Bitmap> > {
        ArrayList<String> thumb;
        public GetThumbnailTask() {

        }

        public GetThumbnailTask(ArrayList<String> list) {
            thumb = list;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
            ObjectRow object = new ObjectRow();
            final List<String> urlStr = new ArrayList<>();
            Bitmap temp;
            List<Bitmap> bitmap = new ArrayList<>();
            for(int i=0;i < thumb.size();i++ ){
                temp = null;
                temp = getBitmapFromURL(thumb.get(i));
                if(temp == null){
                    temp = getBitmapFromURL(thumb.get(i));
                }else {
                    bitmap.add(temp);

                }
            }
            return bitmap;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            startAnim();
        }
        @Override
        protected void onPostExecute(List<Bitmap> bitmap) {
            super.onPostExecute(bitmap);
            // progressBar.setVisibility(View.GONE);
            people = new ArrayList<>();
            stopAnim();
            for(int i=0 ;i < bitmap.size();i++) {
                thumbPic = bitmap.get(i);
                thumbName = sceneName.get(i);
                people.add(new PersonData(
                        thumbName,
                        thumbPic
                ));

            }
            adapter = new MyAdapter(people);
            AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
            alphaAdapter.setDuration(300);
            mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));
            ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    PicViewFragment picViewFragment = new PicViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("sceneName",sceneName.get(position).toString());
                    picViewFragment.setArguments(bundle);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame, picViewFragment);
                    transaction.addToBackStack(null);
                    // Commit the transaction
                    transaction.commit();
                }
            });




            // byte[] thumbnailImage = thumbnail.getDataObject();

        }
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;
        } catch (IOException e) {
            // Log exception

            return null;
        }
    }
    void startAnim(){
        loadingView.setVisibility(View.VISIBLE);
    }

    void stopAnim(){
        loadingView.setVisibility(View.GONE);
    }

}
