package com.theta360.sample;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.util.LruCache;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.OvershootInterpolator;
import android.widget.ProgressBar;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.shaded.fasterxml.jackson.core.JsonParser;
import com.shaded.fasterxml.jackson.databind.deser.DataFormatReaders;
import com.theta360.lib.ThetaException;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator;

public class FromFirebaseActivity extends Activity {
    private FirebaseAuth firebaseAuth;
    private GoogleApiClient googleApiClient;
    private String fUID;
    private ProgressBar progressBar;
    private FirebaseDatabase mDatabase;
    private String itemsUrl;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    private Bitmap thumbPic;
    private String thumbName;
    private ArrayList<String> listReal;
    private List<String> thumbUrl;
    private List<String> realUrl;
    private List<String> keyScene;
    private List<String> keyPic;
    List<Bitmap> savedBitmap;
    private LruCache<String, Bitmap> mMemoryCache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#330000ff")));
        actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#550000ff")));
        actionBar.setTitle("");
        actionBar.setIcon(null);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_firebase);
        //progressBar = (ProgressBar)findViewById(R.id.avloadingIndicatorView);
        final Intent intent = getIntent();
        ArrayList<String> list =
                (ArrayList<String>)intent.getSerializableExtra("thumbUrl");
         listReal =
                (ArrayList<String>)intent.getSerializableExtra("realUrl");
        Log.v("key","   " + list);
        new GetThumbnailTask(list).execute();
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        thumbName = "WOW such Image";
        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
          /*    for(int i= 0; i < position;i++ ){
                  Intent intent1 = new Intent(getApplicationContext(),GridPicActivity.class);
                  intent1.putExtra("inform",new ArrayList<PersonData>(people));
                  startActivity(intent1);

                }*/
                Intent intent1 = new Intent(getApplicationContext(),SimpleVrPanoramaActivity.class);
                intent1.putExtra("clickedItem",listReal.get(position).toString());
                startActivity(intent1);

                // do it
            }
        });
    }
    private class GetThumbnailTask extends AsyncTask<Void, Void, List<Bitmap> > {
        ArrayList<String> thumb;
        public GetThumbnailTask() {

        }

        public GetThumbnailTask(ArrayList<String> list) {
            thumb = list;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
            ObjectRow object = new ObjectRow();
            final List<String> urlStr = new ArrayList<>();
            Bitmap temp;
            List<Bitmap> bitmap = new ArrayList<>();
            for(int i=0;i < thumb.size();i++ ){
                temp = null;
                temp = getBitmapFromURL(thumb.get(i));
                if(temp == null){
                    temp = getBitmapFromURL(thumb.get(i));
                }else {
                    bitmap.add(temp);

                }
            }
            return bitmap;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            startAnim();
        }
        @Override
        protected void onPostExecute(List<Bitmap> bitmap) {
            super.onPostExecute(bitmap);
           // progressBar.setVisibility(View.GONE);
            people = new ArrayList<>();
            stopAnim();
                for(int i=0;i < bitmap.size();i++) {

                    thumbPic = bitmap.get(i);
                    addBitmapToMemoryCache(String.valueOf(i), bitmap.get(i));
                    thumbName = i + " PIC ";
                    people.add(new PersonData(
                            thumbName,
                            thumbPic
                    ));

                }
                adapter = new MyAdapter(people);
                AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(500);
                mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));

                Log.v("key","Sumtin wong  ggggg");



            // byte[] thumbnailImage = thumbnail.getDataObject();

        }
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;
        } catch (IOException e) {
            // Log exception

            return null;
        }
    }
    void startAnim(){
        findViewById(R.id.avloadingIndicatorView).setVisibility(View.VISIBLE);
    }

    void stopAnim(){
        findViewById(R.id.avloadingIndicatorView).setVisibility(View.GONE);
    }
    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
    public int getBitmapFromMemCacheNum() {
        return mMemoryCache.size();
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        for(int i=0;i < getBitmapFromMemCacheNum();i++){

            thumbPic = getBitmapFromMemCache(String.valueOf(i));
            thumbName = i + " PIC ";
            people.add(new PersonData(
                    thumbName,
                    thumbPic
            ));
        }
        adapter = new MyAdapter(people);
        AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
        alphaAdapter.setDuration(500);
        mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("STATE-SAVE", "onSaveInstanceState()");
    }
}
