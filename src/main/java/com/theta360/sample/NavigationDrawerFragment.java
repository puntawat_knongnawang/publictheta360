package com.theta360.sample;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {
private String sceneName;
    private  String ownerName;
    private String survayerName;
    private String locationName;
    private Button nextButton;
    private EditText editScene;
    private EditText editOwner;
    private EditText editSurvayer;
    private EditText editLocation;
    private DatePicker datePicker;
    private int mYear;
    private int mMonth;
    private int mDay;
    private Toast toast;

private boolean isNull = false ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigate, container, false);
        editScene = (EditText)v.findViewById(R.id.scene);
        editOwner = (EditText)v.findViewById(R.id.owner);
        editSurvayer = (EditText)v.findViewById(R.id.survayer);
        editLocation = (EditText)v.findViewById(R.id.location);
        datePicker = (DatePicker)v.findViewById(R.id.datePicker) ;
        nextButton = (Button)v.findViewById(R.id.buttonNext) ;
        isNull = false;
        editScene.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.equals(null)){
                    sceneName = editable.toString();

                }else nullEdit();

            }
        });
        editOwner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.equals(null)){
                    ownerName = editable.toString();
                }else nullEdit();
            }
        });
        editSurvayer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.equals(null) ){
                    survayerName = editable.toString();
                }else nullEdit();
            }
        });
        editLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!editable.equals(null)){
                    locationName = editable.toString();
                }else nullEdit();
            }
        });
        if(editScene.equals(null)||editOwner.equals(null)||editSurvayer.equals(null)||editLocation.equals(null)){
            isNull = true;
        }
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNull == true){
                    Toast.makeText(getContext(),"Please Enter all information",Toast.LENGTH_SHORT).show();
                }else  {

                    try {
                        mDay = datePicker.getDayOfMonth();
                        mMonth = datePicker.getMonth() + 1;
                        mYear = datePicker.getYear();
                        Log.v("input","Scene name : " + sceneName);
                        Log.v("input","Owner name : " + ownerName);
                        Log.v("input","Survay name : " + survayerName);
                        Log.v("input","Location name : " + locationName);
                        Log.v("input","Date : "+mDay + "/" + mMonth +"/" + mYear);
                        ProjectFragment projFrag = new ProjectFragment();
                        /*Bundle bundle = new Bundle();
                        bundle.putString("sceneName",sceneName);
                        bundle.putString("ownerName",ownerName);
                        bundle.putString("survayerName",survayerName);
                        bundle.putString("locationName",locationName);
                        bundle.putString("date",mDay + "/" + mMonth +"/" + mYear);*/
                        String dateP = mDay + "/" + mMonth +"/" + mYear;
                        SurvayDetail detail = new SurvayDetail(sceneName,ownerName,survayerName,dateP,locationName);
                        ((NavigateActivity)getActivity()).setSurDetail(detail);


//                        projFrag.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame, projFrag);
                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(),"Please Enter all information",Toast.LENGTH_SHORT).show();
                    }

                }
                ProjectFragment projFrag = new ProjectFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                transaction.replace(R.id.frame, projFrag);
                transaction.addToBackStack(null);
                // Commit the transaction
                transaction.commit();

            }
        });
        return v;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }
    public void nullEdit(){

        isNull = true;
    }

}
