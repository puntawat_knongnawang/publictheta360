package com.theta360.sample;

/**
 * Created by Admin on 12/7/2016.
 */
public interface MyEventListener {
    public void onEvent(SurvayDetail detail);
}
