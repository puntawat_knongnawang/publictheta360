package com.theta360.sample.view;

/*created using Android Studio (Beta) 0.8.14
www.101apps.co.za*/

import android.graphics.Bitmap;

public class PersonData {
    public PersonData() {
    }

    String name;
    String email;
    Bitmap image;
    int id_;

    public PersonData(String name, Bitmap image) {
        this.name = name;
//        this.email = email;
        this.image = image;
//        this.id_ = id_;
    }


    public String getName() {
        return name;
    }


//    public String getEmail() {
//        return email;
//    }


    public Bitmap getImage() {
        return image;
    }

/*
    public int getId() {
        return id_;
    }
}*/
}