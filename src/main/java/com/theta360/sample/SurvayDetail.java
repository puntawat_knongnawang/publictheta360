package com.theta360.sample;

/**
 * Created by Admin on 12/7/2016.
 */
public class SurvayDetail {
    private String sceneName;
    private  String ownerName;
    private String survayerName;
    private String locationName;
    private String dateDay;


    public SurvayDetail(String scene,String owner,String survayer,String date,String location) {
        sceneName = scene;
        ownerName = owner;
        survayerName = survayer;
        dateDay = date;
        locationName = location;
    }

    public SurvayDetail() {

    }

    public String getSurvayerName() {
        return survayerName;
    }

    public String getDate() {
        return dateDay;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getSceneName() {
        return sceneName;
    }

    public void setSurvayerName(String survayerName) {
        this.survayerName = survayerName;
    }

    public void setDate(String date) {
        this.dateDay = date;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setSceneName(String sceneName) {
        this.sceneName = sceneName;
    }




}
