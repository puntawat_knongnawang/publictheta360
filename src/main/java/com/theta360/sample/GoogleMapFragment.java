package com.theta360.sample;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;
import com.theta360.sample.view.ObjectRow;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class GoogleMapFragment extends Fragment implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerDragListener, GoogleMap.OnInfoWindowCloseListener, GoogleMap.OnInfoWindowLongClickListener {
    MapView mMapView;
    private GoogleMap googleMap;
    private GoogleApiClient mGoogleApiClient;
    private ArrayList<String> testInfoImmage;
    private ArrayList<ObjectRow> objectThumb;
    private ArrayList<ObjectRow> objectReal;
    private Location location;
    private String sceneName;
    private String ownerName;
    private String survayerName;
    private String locationName;
    private String date;
    private LatLng curLatLn;
    SurvayDetail suyDetail;
    private Button finishBut;
    private ArrayList<LatLng> latLngs;
    private MyReceiver myReceiver;
    private String ssIdTHETA;
    private String passTHETA;
    private WifiManager wifi;
    private List<ScanResult> results;
    private int camCount = 0;
    private ArrayList<String> names;
    private boolean isAlertDialogShown = false;
    private ListView lv, lv1;
    private Button backBut;

    private Location currentBestLocation = null;
    public GoogleMapFragment() {
        // Required empty public constructor
       /* testInfoImmage = new ArrayList<>();
        testInfoImmage.add("http://vision.princeton.edu/projects/2012/SUN360/CVPR2012code/data/sun360/image/outdoor/beach/pano1024x512/pano_aagyyrcbytuehj.jpg");
        testInfoImmage.add("http://vision.princeton.edu/projects/2012/SUN360/CVPR2012code/data/sun360/image/outdoor/beach/pano1024x512/pano_aamyrqnbetowsy.jpg");
        testInfoImmage.add("http://vision.princeton.edu/projects/2012/SUN360/CVPR2012code/data/sun360/image/outdoor/beach/pano1024x512/pano_abhnefiriurodl.jpg");*/
        suyDetail = new SurvayDetail();
    }

    private LocationManager mLocationManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_google_map, container, false);
        mLocationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
       /* mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000,
                10000, mLocationListener);
        location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);*/
        location = getLastBestLocation();
        MapsInitializer.initialize(this.getActivity());
        // Inflate the layout for this fragment
        backBut = (Button)v.findViewById(R.id.buttonBackToProj) ;
        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               getFragmentManager().popBackStackImmediate();
            }
        });
        objectThumb = new ArrayList<>();
        objectReal = new ArrayList<>();
        curLatLn = new LatLng(location.getLatitude(), location.getLongitude());
        suyDetail = ((NavigateActivity) getActivity()).getSurDetail();
        finishBut = (Button) v.findViewById(R.id.button_finish);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            objectThumb = bundle.getParcelableArrayList("objectThumb");
            objectReal = bundle.getParcelableArrayList("objectReal");
            sceneName = suyDetail.getSceneName();
            ownerName = suyDetail.getOwnerName();
            survayerName = suyDetail.getSurvayerName();
            locationName = suyDetail.getLocationName();
            date = suyDetail.getDate();
            Log.v("input3", "Scene name : " + sceneName);
            Log.v("input3", "Owner name : " + ownerName);
            Log.v("input3", "Survay name : " + survayerName);
            Log.v("input3", "Location name : " + locationName);
            Log.v("input3", "Date : " + date);
        } else {

            Toast.makeText(getContext(), "IT NULL MF", Toast.LENGTH_SHORT);


        }


        Log.v("latln", curLatLn.toString());
        latLngs = new ArrayList<>();
        for (int i = 0; i < objectReal.size(); i++) {
            latLngs.add(curLatLn);
        }
        finishBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Log.v("marker", " latlns : " + latLngs);
                for(int i = 0;i<objectReal.size();i++) {

                }*/
                reconnectWifi();
                ChooseLogoFragment chooseLogoFragment = new ChooseLogoFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("objectThumb", objectThumb);
                bundle.putParcelableArrayList("objectReal", objectReal);
                bundle.putString("sceneName", sceneName);
                bundle.putString("ownerName", ownerName);
                bundle.putString("survayerName", survayerName);
                bundle.putString("locationName", locationName);
                bundle.putString("date", date);
                bundle.putParcelableArrayList("latln", latLngs);
                chooseLogoFragment.setArguments(bundle);
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, chooseLogoFragment);
                transaction.addToBackStack(null);
                // Commit the transaction
                transaction.commit();
            }
        });
        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            curLatLn = new LatLng(location.getLatitude(), location.getLongitude());
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    public void onMapReady(GoogleMap map) {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true);
        map.setIndoorEnabled(true);
        map.setBuildingsEnabled(true);
        map.setContentDescription("Map with lots of markers.");
        map.getUiSettings().setZoomControlsEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(curLatLn, 19));
        for (int i = 0; i < objectReal.size(); i++) {
            map.addMarker(new MarkerOptions()
                    .position(curLatLn)
                    .title(objectReal.get(i).getFileName())
                    .draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher))

            );

        }

        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                Log.v("marker", marker.getId());

                View v = getActivity().getLayoutInflater().inflate(R.layout.windowlayout, null);
                ImageView imageView = (ImageView) v.findViewById(R.id.info_image);
                for (int i = 0; i < objectReal.size(); i++) {
                    String compar = new String();
                    compar = "m" + i;
                    //Log.v("marker"," Compare : " + compar) ;
                    if (marker.getId().equals(compar)) {
                        byte[] test = objectReal.get(i).getThumbnail();
                        Bitmap realPic = BitmapFactory.decodeByteArray(test, 0, test.length);
//                        Picasso.with(getContext()).load(testInfoImmage.get(i)).into(imageView);
                       /* Glide.with(getContext()).load(realPic).placeholder(R.drawable.progress_activity_dont)
                                .error(R.drawable.error).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);*/
                        imageView.setImageBitmap(realPic);
                    }
                }


                return v;
            }
        });
        map.setOnMarkerClickListener(this);
        map.setOnInfoWindowClickListener(this);
        map.setOnMarkerDragListener(this);
        map.setOnInfoWindowCloseListener(this);
        map.setOnInfoWindowLongClickListener(this);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.isInfoWindowShown()) {
            marker.hideInfoWindow();
        } else {
            marker.showInfoWindow();
        }

        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(final Marker marker) {

        for (int i = 0; i < objectReal.size(); i++) {
            String compar = new String();
            compar = "m" + i;
            //Log.v("marker", " Compare : " + compar);
            if (marker.getId().equals(compar)) {
                latLngs.set(i, marker.getPosition());

            }
        }

    }

    @Override
    public void onInfoWindowClose(Marker marker) {

    }

    @Override
    public void onInfoWindowLongClick(Marker marker) {

    }
    public void disconnectWifi() {

        WifiManager wifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }

    }

    public void reconnectWifi() {

        WifiManager wifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        wifiManager.setWifiEnabled(true);

    }
    public void connectCamWifi() {

        wifi = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);

        //Enable Wifi
        if (!wifi.isWifiEnabled()) {
            Toast.makeText(getContext(), "Wifi is disabled..making it enabled",
                    Toast.LENGTH_LONG).show();
            Log.v("WifiLOG", "ENABLE WIFI");
            wifi.setWifiEnabled(true);
        }

        //Register receiver
        myReceiver = new MyReceiver();
        getContext().registerReceiver(myReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        wifi.startScan();

    }
    private void connectToSSID(String ssid, String password) {
        getContext().unregisterReceiver(myReceiver);

        Log.v("WifiLOG", "START CONNECTING TO THETA WIFI ID:" + ssid + " PASS:" + password);
        WifiConfiguration ezconf = new WifiConfiguration();
        ezconf.priority = 40;
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
        ezconf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        ezconf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
        ezconf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
        ezconf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);

        ezconf.SSID = "\"" + ssid + "\"";
        ezconf.preSharedKey = "\"".concat(password).concat("\"");

        WifiManager wfMgr = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);
        int networkId = wfMgr.addNetwork(ezconf);
        if (networkId != -1) {
            wfMgr.enableNetwork(networkId, true);
        }
    }
    public class MyReceiver extends BroadcastReceiver {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void onReceive(Context context, Intent intent) {
            results = wifi.getScanResults();
            names = new ArrayList<String>();
            camCount = 0;

            //Loopping List to find THETA wifi
            for (int w = 0; w < results.size(); w++) {
                if (results.get(w).SSID.contains("THETA")) {
                    names.add((results.get(w)).SSID);
                    camCount++;
                }
            }

            //Case for camCount
            if (camCount > 1) {
                alertDialogWifi();

            } else if (camCount == 1) {
                //Check THETA
                ssIdTHETA = names.get(0);
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }
                connectToSSID(ssIdTHETA, passTHETA);

            } else if (camCount == 0) {
                Toast.makeText(getContext(), "No Camera Available :(",
                        Toast.LENGTH_LONG).show();
            }

        }
    }
    public void alertDialogWifi() {
        final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View convertView = (View) inflater.inflate(R.layout.custom, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle("Choose the camera.");


        lv1 = (ListView) convertView.findViewById(R.id.listView1);
        lv1.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.custom_tv, names));

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // WiFi selected

                ssIdTHETA = (String) ((TextView) view).getText();
                if (ssIdTHETA.length() == 15) {
                    //THETA M15
                    passTHETA = ssIdTHETA.substring(7, ssIdTHETA.length());
                } else if (ssIdTHETA.length() == 11) {
                    //THETA
                    passTHETA = "00" + ssIdTHETA.substring(5, ssIdTHETA.length());
                }

                Toast.makeText(getContext(), "" + ssIdTHETA + " " + passTHETA,
                        Toast.LENGTH_LONG).show();

                connectToSSID(ssIdTHETA, passTHETA);

                alertDialog.dismiss();
                isAlertDialogShown = false;
            }
        });
        if (!isAlertDialogShown) {
            alertDialog.show();
            isAlertDialogShown = true;
        }
        Log.v("WifiLOG", "OPEN DIALOG WIFI");

    }
    static final int TWO_MINUTES = 1000 * 60 * 2;
    private Location getLastBestLocation() {
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) { GPSLocationTime = locationGPS.getTime(); }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if ( 0 < GPSLocationTime - NetLocationTime ) {
            return locationGPS;
        }
        else {
            return locationNet;
        }
    }
}


