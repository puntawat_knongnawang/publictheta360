package com.theta360.sample;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.Toast;

import com.theta360.lib.PtpipInitiator;
import com.theta360.lib.ptpip.connector.PtpipConnector;
import com.theta360.lib.ptpip.entity.DeviceInfo;
import com.theta360.lib.ptpip.entity.ObjectHandles;
import com.theta360.lib.ptpip.entity.ObjectInfo;
import com.theta360.lib.ptpip.entity.PtpObject;
import com.theta360.lib.ptpip.entity.StorageIds;
import com.theta360.lib.ptpip.entity.StorageInfo;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.net.SocketFactory;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class ProjectFragment extends Fragment {
private Button buttonCamera;
    private Button buttonNewPic;
    private Button buttonNextGoogle;
    private ArrayList<ObjectRow> objectRowFromfrag ;
    private ArrayList<ObjectRow> sendThumb;
    private ArrayList<ObjectRow> sendReal;
    private View loadingView;
    private String cameraIpAddress;
    private ObjectHandles objectHandles;
    private ObjectRow objectRow;
    private String sceneName;
    private  String ownerName;
    private String survayerName;
    private String locationName;
    private String date;

    private static ArrayList<PersonData> people;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_project, container, false);

        cameraIpAddress = getResources().getString(R.string.theta_ip_address);
        buttonCamera = (Button)v.findViewById(R.id.buttonFromCamera);
        buttonNextGoogle = (Button)v.findViewById(R.id.buttonNextGoogleMap);
        loadingView = (View)v.findViewById(R.id.loading_project) ;
        sendThumb = new ArrayList<>();
        sendReal = new ArrayList<>();
        mRecyclerView = (RecyclerView) v.findViewById(R.id.object_list3);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        buttonNewPic = (Button)v.findViewById(R.id.buttonTakePic);
        buttonNewPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TakePicFragmen camFrag = new TakePicFragmen();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, camFrag);
                transaction.addToBackStack(null);
                // Commit the transaction
                transaction.commit();
            }
        });
        buttonNextGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Toast.makeText(getContext(),"Please select picture.",Toast.LENGTH_SHORT);
              /*  reconnectWifi();
                GoogleMapFragment toGGmap = new GoogleMapFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame,  toGGmap);
                transaction.addToBackStack(null);
                // Commit the transaction
                transaction.commit();*/
            }
        });
            if(getArguments() != null){
                Bundle bundle = this.getArguments();
                if(bundle.getParcelableArrayList("objectRows2") != null){
                    objectRowFromfrag = bundle.getParcelableArrayList("objectRows2");
                    new LoadRealObjectListTask(objectRowFromfrag ).execute();
                    new LoadObjectListTask(objectRowFromfrag ).execute();
                }
                /*sceneName =bundle.getString("sceneName");
                ownerName =bundle.getString("ownerName");
                survayerName =bundle.getString("survayerName");
                locationName =bundle.getString("locationName");
                date =bundle.getString("date");*/
                Log.v("input2","Scene name : " + sceneName);
                Log.v("input2","Owner name : " + ownerName);
                Log.v("input2","Survay name : " + survayerName);
                Log.v("input2","Location name : " + locationName);
                Log.v("input2","Date : "+date);

            }


       /* Log.v("input4","Scene name : " + sceneName);
        Log.v("input4","Owner name : " + ownerName);
        Log.v("input4","Survay name : " + survayerName);
        Log.v("input4","Location name : " + locationName);
        Log.v("input4","Date : "+date);*/
        buttonCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FromCameraFragment camFrag = new FromCameraFragment();
                /*Bundle bundle = new Bundle();
                bundle.putString("sceneName",sceneName);
                bundle.putString("ownerName",ownerName);
                bundle.putString("survayerName",survayerName);
                bundle.putString("locationName",locationName);
                bundle.putString("date",date);

                camFrag.setArguments(bundle);*/
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame, camFrag);
                transaction.addToBackStack(null);
                // Commit the transaction
                transaction.commit();
            }
        });


        return v;
    }



    private class LoadObjectListTask extends AsyncTask<Void, String, List<ObjectRow>> {
        ObjectRow obj;
        ArrayList<ObjectRow> objs;
        private ObjectRow objectRow2;
        public LoadObjectListTask(ArrayList<ObjectRow> objectRows) {
      objs = objectRows;
        }

        @Override
        protected void onPreExecute() {
            //startAnim();
        }

        @Override
        protected List<ObjectRow> doInBackground(Void... params) {
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);
                objectHandles = camera.getObjectHandles(PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT);
                int objectHandleCount = objectHandles.size();
                List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
                for(int i = 0;i<objs.size();i++){
                    Log.v("test", "  Size : " +String.valueOf(objs.size()));
                    obj = objs.get(i);
                    for (int x = 0; x < objectHandleCount; x++) {
                        objectRow = new ObjectRow();
                        final int objectHandle = objectHandles.getObjectHandle(x);
                        ObjectInfo object = camera.getObjectInfo(objectHandle);
                        Log.v("test", "   objR : " +obj.getFileName() + " x:"+ x + " "  + object.getFilename());
                        objectRow.setObjectHandle(objectHandle);
                        if(obj.getFileName().equals(object.getFilename())){
                            objectRow.setFileName(object.getFilename());
                            objectRow.setCaptureDate(object.getCaptureDate());
                            if (object.getObjectFormat() == ObjectInfo.OBJECT_FORMAT_CODE_EXIF_JPEG) {
                                objectRow.setIsPhoto(true);
                                PtpObject thumbnail = camera.getThumb(objectHandle);
                                final byte[] thumbnailImage = thumbnail.getDataObject();
                                objectRow.setThumbnail(thumbnailImage);

                            } else {
                                objectRow.setIsPhoto(false);
                            }

                            objectRows.add(objectRow);

                            Log.v("test", "  Size objR : " +objectRows.size());
                        }


                    }
                }

                return objectRows;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {

            }
        }

        @Override
        protected void onPostExecute(final List<ObjectRow> objectRows) {
            if (objectRows != null) {
                people = new ArrayList<>();
                sendThumb = (ArrayList<ObjectRow>) objectRows;
                Log.v("Err", String.valueOf(objectRows.size()));
                for (int i = 0; i < objectRows.size(); i++) {
                    byte[] test = objectRows.get(i).getThumbnail();
                    Bitmap thumbPic = BitmapFactory.decodeByteArray(test, 0, test.length);
                    String thumbName = objectRows.get(i).getFileName().toString();
                    Log.v("capdate : ",objectRows.get(i).getCaptureDate());
                    people.add(new PersonData(
                            thumbName,
                            thumbPic
                    ));
                }
                adapter = new MyAdapter(people);
                final AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
                alphaAdapter.setDuration(420);
                mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));


            }


           // stopAnim();
        }

        @Override
        protected void onCancelled() {
           // stopAnim();
        }
    }
    private class LoadRealObjectListTask extends AsyncTask<Void, String, List<ObjectRow>> {
        ObjectRow obj;
        ArrayList<ObjectRow> objs;
        private ObjectRow objectRow2;
        public LoadRealObjectListTask(ArrayList<ObjectRow> objectRows) {
            objs = objectRows;
        }

        @Override
        protected void onPreExecute() {
            startAnim();
        }

        @Override
        protected List<ObjectRow> doInBackground(Void... params) {
            try {
                PtpipInitiator camera = new PtpipInitiator(getWifiSocketFactory(), cameraIpAddress);
                objectHandles = camera.getObjectHandles(PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT, PtpipInitiator.PARAMETER_VALUE_DEFAULT);
                int objectHandleCount = objectHandles.size();
                List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
                for(int i = 0;i<objs.size();i++){
                    Log.v("test", "  Size : " +String.valueOf(objs.size()));
                    obj = objs.get(i);
                    for (int x = 0; x < objectHandleCount; x++) {
                        objectRow = new ObjectRow();
                        final int objectHandle = objectHandles.getObjectHandle(x);
                        ObjectInfo object = camera.getObjectInfo(objectHandle);
                        Log.v("test", "   objR : " +obj.getFileName() + " x:"+ x + " "  + object.getFilename());
                        objectRow.setObjectHandle(objectHandle);
                        if(obj.getFileName().equals(object.getFilename())){
                            objectRow.setFileName(object.getFilename());
                            objectRow.setCaptureDate(object.getCaptureDate());

                            if (object.getObjectFormat() == ObjectInfo.OBJECT_FORMAT_CODE_EXIF_JPEG) {
                                objectRow.setIsPhoto(true);
                                PtpObject realPic = camera.getResizedImageObject(objectHandle, 2048, 1024);
                                final byte[] realImage = realPic.getDataObject();
                                objectRow.setThumbnail(realImage);


                            } else {
                                objectRow.setIsPhoto(false);
                            }
                            objectRows.add(objectRow);
                            Log.v("test", "  Size objR : " +objectRows.size());
                        }
                    }
                }

                return objectRows;

            } catch (Throwable throwable) {
                String errorLog = Log.getStackTraceString(throwable);
                publishProgress(errorLog);
                return null;
            }
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {

            }
        }

        @Override
        protected void onPostExecute(final List<ObjectRow> objectRows) {
            if (objectRows != null) {
                people = new ArrayList<>();
                sendReal = (ArrayList<ObjectRow>) objectRows;
                buttonNextGoogle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        reconnectWifi();

                        GoogleMapFragment toGGmap = new GoogleMapFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelableArrayList("objectThumb", sendThumb);
                        bundle.putParcelableArrayList("objectReal",  sendReal);
                        bundle.putString("sceneName",sceneName);
                        bundle.putString("ownerName",ownerName);
                        bundle.putString("survayerName",survayerName);
                        bundle.putString("locationName",locationName);
                        bundle.putString("date",date);
                        toGGmap.setArguments(bundle);
                        FragmentTransaction transaction = getFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame,  toGGmap);
                        transaction.addToBackStack(null);
                        // Commit the transaction
                        transaction.commit();
                    }
                });

            }


            stopAnim();
        }

        @Override
        protected void onCancelled() {
            stopAnim();
        }
    }
    private SocketFactory getWifiSocketFactory() {
        SocketFactory socketFactory = SocketFactory.getDefault();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && !isGalaxyDevice()) {
            ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            Network[] allNetwork = cm.getAllNetworks();
            for (Network network : allNetwork) {
                NetworkCapabilities networkCapabilities = cm.getNetworkCapabilities(network);
                if (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    socketFactory = network.getSocketFactory();
                }
            }
        }
        return socketFactory;
    }

    private static final String BRAND_SAMSUNG = "samsung";
    private static final String MANUFACTURER_SAMSUNG = "samsung";

    private boolean isGalaxyDevice() {
        if ((Build.BRAND != null) && (Build.BRAND.toLowerCase(Locale.ENGLISH).contains(BRAND_SAMSUNG))) {
            return true;
        }
        if ((Build.MANUFACTURER != null) && (Build.MANUFACTURER.toLowerCase(Locale.ENGLISH).contains(MANUFACTURER_SAMSUNG))) {
            return true;
        }
        return false;
    }
    public void reconnectWifi() {

        WifiManager wifiManager = (WifiManager) getContext().getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(false);
        }
        wifiManager.setWifiEnabled(true);

    }
    void startAnim() {
        loadingView.setVisibility(View.VISIBLE);
    }

    void stopAnim() {
        loadingView.setVisibility(View.GONE);
    }
}
