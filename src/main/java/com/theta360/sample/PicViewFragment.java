package com.theta360.sample;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.theta360.sample.view.ObjectRow;
import com.theta360.sample.view.PersonData;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator;


public class PicViewFragment extends Fragment {
    private String sceneName;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private static RecyclerView.Adapter adapter;
    private static ArrayList<PersonData> people;
    private FirebaseAuth firebaseAuth;
    private FirebaseDatabase mDatabase;
    private  String itemsUrl;
    private View loadingView;
    private Handler handler = new Handler();
    private ArrayList<String> thumbUrl;
    private ArrayList<String> realUrl;
    private Bitmap thumbPic;
    private String thumbName;
    public PicViewFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_pic_view, container, false);
        if(getArguments() != null){
            Bundle bundle = getArguments();
            sceneName = bundle.getString("sceneName");

            Log.v("input3","Scene name : " + sceneName);

        }else{
            Toast.makeText(getContext(),"IT NULL MF",Toast.LENGTH_SHORT);
        }
        thumbUrl = new ArrayList<>();
        realUrl = new ArrayList<>();
        loadingView = (View)v.findViewById(R.id.loading_pic);
        mRecyclerView = (RecyclerView)v.findViewById(R.id.object_list_pic) ;
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new GridLayoutManager(getContext(),2);
        SlideInLeftAnimator animator = new SlideInLeftAnimator();
        animator.setInterpolator(new OvershootInterpolator(1f));
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(animator);
        itemsUrl = "https://nextweaverproject.firebaseio.com" ;
        mDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = mDatabase.getReferenceFromUrl(itemsUrl);
        myRef.child("Scenes").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot children : dataSnapshot.getChildren()) {
                    Log.v("key", "   " + children.getKey());
                    Log.v("keyValue", "   " + children.child("SceneName").getValue());
                    if (children.child("SceneName").getValue().equals(sceneName)) {

                        for (DataSnapshot child : children.getChildren()) {
                            Log.v("key1", "   " + child.getKey());
                            if (child.getKey().equals("Thumb")) {
                                for (DataSnapshot childT : child.getChildren()) {
                                    //Log.v("key2","   " + child2.getValue(String.class));
                                    for (DataSnapshot child3 : childT.getChildren()) {
                                        //Log.v("key3","   " + child3.getKey());
                                        if (child3.getKey().equals("LongUrl")) {
                                            Log.v("key4", "   " + child3.getValue(String.class));
                                            String temp = null;
                                            temp = child3.getValue(String.class);
                                            if (temp != null) {
                                                // thumbUrl.add(temp);
                                                thumbUrl.add(temp);
                                            }
                                        }
                                    }
                                }
                            }
                            if (child.getKey().equals("Real")) {
                                for (DataSnapshot childR : child.getChildren()) {
                                    //Log.v("key2","   " + child2.getValue(String.class));
                                    for (DataSnapshot child3 : childR.getChildren()) {
                                        //Log.v("key3","   " + child3.getKey());
                                        if (child3.getKey().equals("LongUrl")) {
                                            Log.v("key4R", "   " + child3.getValue(String.class));
                                            String temp = null;
                                            temp = child3.getValue(String.class);
                                            if (temp != null) {
                                                realUrl.add(temp);
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }




                   // Log.v("keyThumb","   " + thumbUrl);
                }
                handler.postDelayed(runnable, 3000);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        ItemClickSupport.addTo(mRecyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Intent intent1 = new Intent(getContext(),SimpleVrPanoramaActivity.class);
                intent1.putExtra("clickedItem",realUrl.get(position).toString());
                startActivity(intent1);
            }
        });
        return  v;
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            Log.v("storage", thumbUrl.toString());
            new GetThumbnailTask(thumbUrl).execute();
      /* and here comes the "trick" */
             handler.postDelayed(this, 2000);
            if(thumbUrl ==null){
                Log.v("storage", thumbUrl.toString());
                new GetThumbnailTask(thumbUrl).execute();
            }

            handler.removeCallbacks(runnable);

        }
    };

    private class GetThumbnailTask extends AsyncTask<Void, Void, List<Bitmap> > {
        ArrayList<String> thumb;
        public GetThumbnailTask() {

        }

        public GetThumbnailTask(ArrayList<String> list) {
            thumb = list;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<ObjectRow> objectRows = new ArrayList<ObjectRow>();
            ObjectRow object = new ObjectRow();
            final List<String> urlStr = new ArrayList<>();
            Bitmap temp;
            List<Bitmap> bitmap = new ArrayList<>();
            for(int i=0;i < thumb.size();i++ ){
                temp = null;
                temp = getBitmapFromURL(thumb.get(i));
                if(temp == null){
                    temp = getBitmapFromURL(thumb.get(i));
                }else {
                    bitmap.add(temp);

                }
            }
            return bitmap;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //progressBar.setVisibility(View.VISIBLE);
            startAnim();
        }
        @Override
        protected void onPostExecute(List<Bitmap> bitmap) {
            super.onPostExecute(bitmap);
            // progressBar.setVisibility(View.GONE);
            people = new ArrayList<>();
            stopAnim();
            for(int i=0;i < bitmap.size();i++) {
                thumbPic = bitmap.get(i);
                thumbName = "";
                people.add(new PersonData(
                        thumbName,
                        thumbPic
                ));

            }
            adapter = new MyAdapter(people);
            AlphaInAnimationAdapter alphaAdapter = new AlphaInAnimationAdapter(adapter);
            alphaAdapter.setDuration(300);
            mRecyclerView.setAdapter((new ScaleInAnimationAdapter(alphaAdapter)));





            // byte[] thumbnailImage = thumbnail.getDataObject();

        }
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);

            return myBitmap;
        } catch (IOException e) {
            // Log exception

            return null;
        }
    }
    void startAnim(){
        loadingView.setVisibility(View.VISIBLE);
    }

    void stopAnim(){
        loadingView.setVisibility(View.GONE);
    }


}


